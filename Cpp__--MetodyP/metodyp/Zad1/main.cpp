#include <iostream>
#include <cstdio>
#include <cmath>
#define N 55
#define M 65

using namespace std;

class Zaklad1 {
    public:
        float tab_Z1[N]={22, 26, 27, 27, 9,
                        11, 29, 33, 27, 28,
                        30, 31, 14, 33, 30,
                        16, 20, 18, 34, 33,
                        34, 30, 29, 24, 29,
                        29, 27, 42, 54, 60,
                        52, 36, 37, 43, 57,
                        39, 40, 36, 50, 44,
                        33, 31, 42, 49, 32,
                        36, 41, 32, 48, 31,
                        26, 36, 38, 37, 38};
};

class Zaklad2 {
    public:
        float tab_Z2[M]={38, 37, 22, 31, 64,
                        39, 30, 26, 42, 44,
                        46, 41, 27, 34, 39,
                        16, 46, 60, 37, 36,
                        30, 26, 36, 51, 27,
                        32, 26, 20, 41, 27,
                        40, 44, 26, 19, 18,
                        54, 49, 28, 42, 43,
                        39, 38, 41, 50, 24,
                        33, 38, 36, 18, 53,
                        50, 59, 40, 36, 16,
                        36, 39, 48, 46, 58,
                        33, 43, 37, 44, 40};
};

float srednia_a(int ilosc, float *tablica);
float srednia_h(int ilosc, float *tablica);
float wariancja (int ilosc, float *tablica);
float odch_stand (int ilosc, float *tablica);
float odch_sr (int ilosc, float *tablica);
int przedzial(int ilosc, float *tablica);
int max(int ilosc, float *tablica);
int min(int ilosc, float *tablica);
int sortowanie(int ilosc, float *tablica);
int mediana(int ilosc, float *tablica);
int modalna(int ilosc, float *tablica);


int main()
{
    Zaklad1 zI;
    Zaklad2 zII;
//Wypisuje dane Zakladu I
    cout<<"Zaklad I: ";
    for (int i=0;i<N;++i)
       {
            if (i%5!=0)
            cout<<zI.tab_Z1[i]<<" ";
            else
            {
                cout<<"\n";
                cout<<zI.tab_Z1[i]<<" ";
            }
       }
//Wypisuje dane Zakladu II
    cout<<endl<<endl<<"Zaklad II: ";
    for (int i=0;i<M;++i)
       {
            if (i%5!=0)
            cout<<zII.tab_Z2[i]<<" ";
            else
            {
                cout<<"\n";
                cout<<zII.tab_Z2[i]<<" ";
            }
       }
//Podzial na przedzialy
    cout<<endl<<endl<<endl<<"Podzial na 6 przedzialow od 5 do 65 i zsumowanie wartosci tych przedzialow"<<endl<<endl;
    cout<<"Przedzial dla Zaklad I\n";
    przedzial(N,zI.tab_Z1);
    cout<<"\n";
    cout<<"Przedzial dla Zaklad II\n";
    przedzial(M,zII.tab_Z2);
    cout<<"\n";
//Parametry statystyczne
    cout.precision(3+1); //Zaokrąglenie liczb do 2 miejsc po przecinku
    cout<<"Parametry statystyczne\n\n";
    cout<<"Zaklad I: \n";
    cout<<"Srednia arytmetyczna: "<<srednia_a(N,zI.tab_Z1);
    cout<<"\nWariancja: "<<wariancja(N,zI.tab_Z1);
    cout<<"\nOdchylenie standardowe: "<<odch_stand(N,zI.tab_Z1);
    cout<<"\nOdchylenie od sredniej: "<<odch_sr(N,zI.tab_Z1);
    cout<<"\nSrednia harmoniczna: "<<srednia_h(N,zI.tab_Z1);
    cout<<"\nMax: "<<max(N,zI.tab_Z1);
    cout<<"\nMin: "<<min(N,zI.tab_Z1);
    cout<<"\nPosorotwane: ";
    sortowanie(N,zI.tab_Z1);
    cout<<"\nModalna zbioru: "<<modalna(N,zI.tab_Z1);
    cout<<"Mediana zbioru wynosi: "<<mediana(N,zI.tab_Z1);
    cout<<"\n";

    cout<<"\n\nZaklad II: \n";
    cout<<"Srednia arytmetyczna: "<<srednia_a(M,zII.tab_Z2);
    cout<<"\nWariancja: "<<wariancja(M,zII.tab_Z2);
    cout<<"\nOdchylenie standardowe: "<<odch_stand(M,zII.tab_Z2);
    cout<<"\nOdchylenie od sredniej: "<<odch_sr(M,zII.tab_Z2);
    cout<<"\nSrednia harmoniczna: "<<srednia_h(M,zII.tab_Z2)<<"\n";
    cout<<"Max: "<<max(M,zII.tab_Z2)<<"\n";
    cout<<"Min: "<<min(M,zII.tab_Z2);
    cout<<"\nPosorotwane: ";
    sortowanie(M,zII.tab_Z2);
    cout<<"\nModalna zbioru: "<<modalna(M,zII.tab_Z2);
    cout<<"Mediana zbioru wynosi: "<<mediana(M,zII.tab_Z2);

    cout<<endl<<endl;
    return 0;
}


float srednia_a(int ilosc, float *tablica)
{
    float wynik=0;
    for(int i=0;i<ilosc;i++)
    {
        wynik=wynik+tablica[i];
    }
    return wynik/ilosc;
}

float srednia_h(int ilosc, float *tablica)
{
    float wynik=0;
    for(int i=0;i<ilosc;i++)
    {
        wynik=wynik+(1/tablica[i]);
    }
    return ilosc/wynik;
}

float wariancja (int ilosc, float *tablica)
{
    float wariancja=0;
    for (int i=0;i<ilosc;i++)
    {
        wariancja=wariancja+pow((tablica[i]-srednia_a(ilosc,tablica)),2);//funkcja pow() sluzy do potegowania w tym wypadku do potegi 2
    }
    return wariancja/ilosc;
}

float odch_stand (int ilosc, float *tablica)
{
    float odch_s;
    odch_s=sqrt(wariancja(ilosc,tablica));
    return odch_s;
}

float odch_sr (int ilosc, float *tablica)
{
    float odch_sre=0;
    for (int i=1;i<=ilosc;i++)
    {
        odch_sre+=abs((tablica[i]-srednia_a(ilosc,tablica)));//funkcja abs() oblicza wartosc bezwzgledna
    }
    return odch_sre/ilosc;
}

int przedzial(int ilosc, float *tablica)
{
    int suma_15=0;
    int suma_25=0;
    int suma_35=0;
    int suma_45=0;
    int suma_55=0;
    int suma_65=0;
    for (int i=0;i<ilosc;i++)
    {
        if (tablica[i]<15)
            suma_15=suma_15+1;
        else
            if (tablica[i]<25)
            suma_25=suma_25+1;
        else
            if (tablica[i]<35)
            suma_35=suma_35+1;
        else
            if (tablica[i]<45)
            suma_45=suma_45+1;
        else
            if (tablica[i]<55)
            suma_55=suma_55+1;
        else
            if (tablica[i]<65)
            suma_65=suma_65+1;
    }
    cout<<"5 - 15: "<<suma_15<<endl;
    cout<<"15 - 25: "<<suma_25<<endl;
    cout<<"25 - 35: "<<suma_35<<endl;
    cout<<"35 - 45: "<<suma_45<<endl;
    cout<<"45 - 55: "<<suma_55<<endl;
    cout<<"55 - 65: "<<suma_65<<endl;
    cout<<"Razem: "<<suma_15+suma_25+suma_35+suma_45+suma_55+suma_65<<endl;
}

int max(int ilosc, float *tablica)
{
    int max=tablica[0];
    for (int i=0;i<=ilosc;i++)
    {
        if (tablica[i]>max) max=tablica[i];
    } return max;
}

int min(int ilosc, float *tablica)
{
    int min=tablica[0];
    for (int i=0;i<=ilosc-1;i++)
    {
        if (tablica[i]<min) min=tablica[i];
    } return min;
}

//Funkcja sortowanie wymagana jest do wyznaczenia mediany
int sortowanie(int ilosc, float *tablica)
{
    for (int i=0;i<ilosc-1;i++)
    {
        for (int j=0;j<ilosc-1;j++)
        {
            if (tablica[j]>tablica[j+1])
                swap(tablica[j],tablica[j+1]);
        }
    }
    for (int i=0;i<ilosc;i++)
    {
        cout<<tablica[i]<<", ";
    }
}

int mediana(int ilosc, float *tablica)
{
    int media;
    if(ilosc%2==0)
    media=(ilosc/2)-1;
    else
    media=ilosc/2;
    cout<<endl;
    return tablica[media];
}

int modalna(int ilosc, float *tablica)
{
	int licznik=1;
	for (int i=1;i<ilosc;i++)
    {

        if (licznik==0)
        {

            tablica[0]=tablica[i];
            licznik=1;
        }
        else
            if (tablica[0]==tablica[i]) licznik++;
        else licznik--;
    }
    int ile=0;
    if (licznik>0)
        for (int i=0;i<ilosc;i++)
        {
            if (tablica[i]==tablica[0]) ile++;
        }
    if (ile>(ilosc/2)) return tablica[0];
    else return -1;
}





