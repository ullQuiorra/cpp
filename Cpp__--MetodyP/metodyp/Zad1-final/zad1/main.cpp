#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <windows.h>
#define N 55
#define M 65

using namespace std;

class czas_dojazdu {
    public:
        double tab_Zaklad1[N]={22, 26, 27, 27, 9,
                        11, 29, 33, 27, 28,
                        30, 31, 14, 33, 30,
                        16, 20, 18, 34, 33,
                        34, 30, 29, 24, 29,
                        29, 27, 42, 54, 60,
                        52, 36, 37, 43, 57,
                        39, 40, 36, 50, 44,
                        33, 31, 42, 49, 32,
                        36, 41, 32, 48, 31,
                        26, 36, 38, 37, 38};

        double tab_Zaklad2[M]={38, 37, 22, 31, 64,
                        39, 30, 26, 42, 44,
                        46, 41, 27, 34, 39,
                        16, 46, 60, 37, 36,
                        30, 26, 36, 51, 27,
                        32, 26, 20, 41, 27,
                        40, 44, 26, 19, 18,
                        54, 49, 28, 42, 43,
                        39, 38, 41, 50, 24,
                        33, 38, 36, 18, 53,
                        50, 59, 40, 36, 16,
                        36, 39, 48, 46, 58,
                        33, 43, 37, 44, 40};
};

void Dane_zakladu (int ilosc, double *tablica);
void przedzial(int ilosc, double *tablica);
float srednia_a(int ilosc, double *tablica);
double srednia_h(int ilosc, double *tablica);
float wariancja(int ilosc, double *tablica);
float odch_stand(int ilosc, double *tablica);
float odch_sr(int ilosc, double *tablica);
void max_min (int ilosc, double *tablica);
void sortowanie(int ilosc, double *tablica);
int mediana(int ilosc, double *tablica);
void modalna(int ilosc, double *tablica);

int main()
{
    HANDLE hOut;
    czas_dojazdu czas_doj;
    cout.precision(3+1); //Zaokrąglenie liczb do 2 miejsc po przecinku
//Wypisuje dane obu zakladow
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE | FOREGROUND_INTENSITY);
    cout<<"Zaklad I: ";
    Dane_zakladu(N,czas_doj.tab_Zaklad1);
    cout<<"\n\nPrzedzial czasowy: \n";
    przedzial(N,czas_doj.tab_Zaklad1);
    cout<<"\nSrednia arytmetyczna: "<<srednia_a(N,czas_doj.tab_Zaklad1);
    cout<<"\nSrednia harmoniczna: "<<srednia_h(N,czas_doj.tab_Zaklad1);
    cout<<"\nWariancja: "<<wariancja(N,czas_doj.tab_Zaklad1);
    cout<<"\nOdchylenie standardowe: "<<odch_stand(N,czas_doj.tab_Zaklad1);
    cout<<"\nOdchylenie srednie: "<<odch_sr(N,czas_doj.tab_Zaklad1);
    max_min(N,czas_doj.tab_Zaklad1);
    cout<<"\nPosortowana babelkowo tablica: \n";
    sortowanie(N,czas_doj.tab_Zaklad1);
    cout<<"Mediana: "<<mediana(N,czas_doj.tab_Zaklad1);
    cout<<"\n";
    modalna(N,czas_doj.tab_Zaklad1);

    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_INTENSITY);
    cout<<"\n\nZaklad II: ";
    Dane_zakladu(M,czas_doj.tab_Zaklad2);
    cout<<"\n\nPrzedzial czasowy: \n";
    przedzial(M,czas_doj.tab_Zaklad2);
    cout<<"\nSrednia arytmetyczna: "<<srednia_a(M,czas_doj.tab_Zaklad2);
    cout<<"\nSrednia harmoniczna: "<<srednia_h(M,czas_doj.tab_Zaklad2);
    cout<<"\nWariancja: "<<wariancja(M,czas_doj.tab_Zaklad2);
    cout<<"\nOdchylenie standardowe: "<<odch_stand(M,czas_doj.tab_Zaklad2);
    cout<<"\nOdchylenie srednie: "<<odch_sr(M,czas_doj.tab_Zaklad2);
    max_min(M,czas_doj.tab_Zaklad2);
    cout<<"\nPosortowana babelkowo tablica: \n";
    sortowanie(M,czas_doj.tab_Zaklad2);
    cout<<"Mediana: "<<mediana(M,czas_doj.tab_Zaklad2);
    cout<<"\n";
    modalna(M,czas_doj.tab_Zaklad2);

    cout<<"\n\n\n\n\n";
    return 0;
}

void Dane_zakladu (int ilosc, double *tablica)
{
    for (int i=0;i<ilosc;++i)
       {
            if (i%5!=0)
            cout<<tablica[i]<<" ";
            else
            {
                cout<<"\n";
                cout<<tablica[i]<<" ";
            }
       }
}

void przedzial(int ilosc, double *tablica)
{
    int suma_15=0;
    int suma_25=0;
    int suma_35=0;
    int suma_45=0;
    int suma_55=0;
    int suma_65=0;
    for (int i=0;i<ilosc;i++)
    {
        if (tablica[i]<15)
            suma_15=suma_15+1;
        else
            if (tablica[i]<25)
            suma_25=suma_25+1;
        else
            if (tablica[i]<35)
            suma_35=suma_35+1;
        else
            if (tablica[i]<45)
            suma_45=suma_45+1;
        else
            if (tablica[i]<55)
            suma_55=suma_55+1;
        else
            if (tablica[i]<65)
            suma_65=suma_65+1;
    }
    cout<<"5 - 15: "<<suma_15<<endl;
    cout<<"15 - 25: "<<suma_25<<endl;
    cout<<"25 - 35: "<<suma_35<<endl;
    cout<<"35 - 45: "<<suma_45<<endl;
    cout<<"45 - 55: "<<suma_55<<endl;
    cout<<"55 - 65: "<<suma_65<<endl;
    cout<<"Razem: "<<suma_15+suma_25+suma_35+suma_45+suma_55+suma_65<<endl;
}

float srednia_a(int ilosc, double *tablica)
{
    float wynik=0;
    for(int i=0;i<ilosc;i++)
    {
        wynik+=tablica[i];
    }
    return wynik/ilosc;
}

double srednia_h(int ilosc, double *tablica)
{
    double wynik=0;
    for(int i=0;i<ilosc;i++)
    {
        wynik+=(1/tablica[i]);
    }
    return ilosc/wynik;
}

float wariancja (int ilosc, double *tablica)
{
    double wariancja=0;
    for (int i=0;i<ilosc;i++)
    {
        wariancja+=pow((tablica[i]-srednia_a(ilosc,tablica)),2);//funkcja pow() sluzy do potegowania w tym wypadku do potegi 2
    }
    return wariancja/ilosc;
}

float odch_stand (int ilosc, double *tablica)
{
    float odch_s;
    odch_s=sqrt(wariancja(ilosc,tablica));
    return odch_s;
}

float odch_sr (int ilosc, double *tablica)
{
    float odch_sre=0;
    for (int i=1;i<=ilosc;i++)
    {
        odch_sre+=abs((tablica[i]-srednia_a(ilosc,tablica)));//funkcja abs() oblicza wartosc bezwzgledna
    }
    return odch_sre/ilosc;
}

void max_min (int ilosc, double *tablica)
{
    int max=tablica[0];
    for (int i=0;i<=ilosc;i++)
    {
        if (tablica[i]>max) max=tablica[i];
    }

    int min=tablica[0];
    for (int i=0;i<=ilosc-1;i++)
    {
        if (tablica[i]<min) min=tablica[i];
    }

    cout<<"\nMin: "<<min;
    cout<<"\nMax: "<<max;
}

void sortowanie(int ilosc, double *tablica) //Funkcja sortowanie wymagana jest do wyznaczenia mediany
{
    for (int i=0;i<ilosc-1;i++)
    {
        for (int j=0;j<ilosc-1;j++)
        {
            if (tablica[j]>tablica[j+1])
                swap(tablica[j],tablica[j+1]);
        }
    }
    for (int i=0;i<ilosc;i++)
    {
        cout<<tablica[i]<<", ";
    }
}

int mediana(int ilosc, double *tablica)
{
    int median;
    if(ilosc%2==0)
    median=(ilosc/2)-1;
    else
    median=ilosc/2;
    cout<<endl;
    return tablica[median];
}

void modalna(int ilosc, double *tablica)
{
    double ile = 0;
    double moda_kandydat,licznik,moda;
    for(int i=0;i<ilosc;i++)
    {
        moda_kandydat = tablica[i];
        licznik = 0;
        for(int j=0;j<ilosc;j++)
            if(tablica[j] == moda_kandydat) licznik++;
        if(licznik>ile)
        {
            ile=licznik;
            moda=moda_kandydat;
        }
    }
    cout<<"Modalna: "<<moda<<" wystepuje "<<ile<<" razy.";
}
