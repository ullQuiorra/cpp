#include <iostream>
#include <random>
#include <algorithm>
/*
Jakub Homlala
jhomlala@gmail.com
103 IDZ
*/

using namespace std;
int generujPoisson(int []);
int mini(int []);
int maxi(int []);
void szeregPunktowy(int [],int,int);

double generujNormalny(double []);
void czestosc (double []);
void  czestosc2(int [],int,int *);
int sredniaAr(int [],int);
void mediana(int[],int);
void dominanta(int []);
void wariancja(int [],int,int);
void rozstep (int [],int);
void kwartyl(int [],int);
  default_random_engine generator;
  poisson_distribution<int> distribution(5); // rozklad poissona z parametrem rownym 5
  normal_distribution<double> distribution2(10,0.5); // rozklad normalny z srednia 10 i odchyleniem stand. 0.5


int main()
{

  ///////////////////////////////////////////////
  ///////////////////////////////////////////////
  // PODPUNKT A
  ///////////////////////////////////////////////
  ///////////////////////////////////////////////

  //deklaracja tablicy
  int p[100];

  //wygenerowanie liczb wg rozkladu poissona
  p[100] = generujPoisson(p);

  //posortowanie tablicy
  sort(p,p+100);

  //wyznaczenie minimum i maksimum
  int minimum  =  mini(p);
  int maksimum = maxi(p);
  cout << "\n\nMinimum to:";
  cout << minimum << endl;
  cout << "Maksimum to:";
  cout << maksimum << endl;

  //wyznaczenie rozsteptu
  cout << "ROZSTEP:" << maksimum - minimum;

  //wyznaczenie szeregu punktowego
  szeregPunktowy(p,minimum,maksimum);

    cout << "\n\n/////////////////////////////////////"<<endl;
    cout << "/////////////////////////////////////"<<endl;
    cout << "/////////////////////////////////////"<<endl;

  ///////////////////////////////////////////////
  ///////////////////////////////////////////////
  // PODPUNKT B
  ///////////////////////////////////////////////
  ///////////////////////////////////////////////

  double p2[100];
  p2[100] = generujNormalny(p2);

  czestosc(p2);

    cout << "\n\n/////////////////////////////////////"<<endl;
    cout << "/////////////////////////////////////"<<endl;
    cout << "/////////////////////////////////////"<<endl;


  //WYKRES PRZESY�AM W PLIKU AKRUSZA KALUKALCYJNEGO

  ///////////////////////////////////////////////
  ///////////////////////////////////////////////
  // PODPUNKT C
  ///////////////////////////////////////////////
  ///////////////////////////////////////////////
  int danez1[]={
          22, 26, 27, 27, 9,
          11, 29, 33, 27, 28,
          30, 31, 14, 33, 30,
          16, 20, 18, 34, 33,
          34, 30, 29, 24, 29,
          29, 27, 42, 54, 60,
          52, 36, 37, 43, 57,
          39, 40, 36, 50, 44,
          33, 31, 42, 49, 32,
          36, 41, 32, 48, 31,
          26, 36, 38, 37, 38
        };

    int danez2[]={
        38, 37, 22, 31, 64,
        39, 30, 26, 42, 44,
        46, 41, 27, 34, 39,
        16, 46, 60, 37, 36,
        30, 26, 36, 51, 27,
        32, 26, 20, 41, 27,
        40, 44, 26, 19, 18,
        54, 49, 28, 42, 43,
        39, 38, 41, 50, 24,
        33, 38, 36, 18, 53,
        50, 59, 40, 36, 16,
        36, 39, 48, 46, 58,
        33, 43, 37, 44, 40
        };

    int z1czestosc[6];
    int z2czestosc[6];
    cout << "\n\nZAKLAD 1:" << endl;
    czestosc2(danez1,55,z1czestosc);
    int z1srednia = sredniaAr(z1czestosc,55);
    mediana(z1czestosc,55);
    dominanta(z1czestosc);
    wariancja(z1czestosc,55,z1srednia);
    rozstep(danez1,55);
    kwartyl(danez1,55);

    cout << "\n\n/////////////////////////////////////"<<endl;
    cout << "/////////////////////////////////////"<<endl;
    cout << "/////////////////////////////////////"<<endl;

    cout << "\n\nZAKLAD 2:" << endl;
    czestosc2(danez2,65,z2czestosc);
    int z2srednia = sredniaAr(z2czestosc,65);
    mediana(z2czestosc,65);
    dominanta(z2czestosc);
    wariancja(z2czestosc,65,z2srednia);
    rozstep(danez2,65);
    kwartyl(danez2,65);
  return 0;





}

int generujPoisson(int tab[100])
{
    cout << "GENEROWANIE LICZB (ROZKLAD POISSONA):" << endl;
    for (int i=0; i<100; ++i)
    {
    tab[i]=distribution(generator);
    cout.precision(3);
    cout <<"[" << i+1 <<"]: " << tab[i]<<endl;
    }

    return  *tab;
}
int mini(int tab[100])
{
    int aktualne = tab[0];
    for (int i=0;i<100;i++)
    {
        if (aktualne > tab[i])
        {
            aktualne = tab[i];
        }

    }

return aktualne;

}

int maxi(int tab[100])
{
    int aktualne = tab[0];
    for (int i=0;i<100;i++)
    {
        if (aktualne < tab[i])
        {
            aktualne = tab[i];
        }

    }

return aktualne;

}

void szeregPunktowy(int tab[100],int a,int b)
{
    int licznik;
    cout << "\n\nSZERG PRZEDZIALOWY PUNKTOWY:\nLICZBA ILOSC WYSTAPIEN"<<endl;
    for (int i = a;i<=b;i++)
    {

        licznik = 0;
        for (int k=0;k<100;k++)
            {
                if (tab[k]==i)
                licznik++;
            }
            cout.width(5);
            cout << i << "          " << licznik << endl;

    }


}

double generujNormalny(double tab[100])
{
    cout << "\n\nGENEROWANIE LICZB (ROZKLAD NORMALNY):" << endl;
    for (int i=0; i<100; ++i)
    {
    tab[i]=distribution2(generator);
    cout <<"[" << i+1 <<"]: " << tab[i]<<endl;
    }

    return  *tab;
}

void czestosc (double tab[])
{
// przedzialy:
// ( nieskoczonosc , 8.0 )
// < 8.0 , 8.5)
// <8.5 , 9.0 )
// <9.0 , 9.5)
// <9.5 , 10.0)
// <10 , 10.5 )
// << 10.5 , 11)
// << 11 - nieskonczonosc)

    cout << "\n\nSZEREGI SKUMOLOWNAE PRZEDZIALOWE"<<endl;

    cout << "\nPRZEDZIAL:ILOSC\n"<<endl;


    int licznik_czestosci[8]={0,0,0,0,0,0,0};
    string przedzialy[8] = {"(niesk,8.0)","<8.0,8.5)","<8.5,9.0)","<9.0,9.5)","<9.5,10.0)","<10.0,10.5)","<10.5,11)","<11,niesk)"  };

    for (int i=0;i<100;i++)
    {
        if (tab[i] < 8.0)
            licznik_czestosci[0]++;
        if (tab[i] >= 8.0 && tab[i] < 8.5)
            licznik_czestosci[1]++;
        if (tab[i] >= 8.5 && tab[i] < 9.0)
            licznik_czestosci[2]++;
        if (tab[i] >= 9.0 && tab[i] < 9.5)
            licznik_czestosci[3]++;
        if (tab[i] >= 9.5 && tab[i] < 10)
            licznik_czestosci[4]++;

        if (tab[i] >= 10 && tab[i] < 10.5)
            licznik_czestosci[5]++;
        if (tab[i] >= 10.5 && tab[i] < 11)
            licznik_czestosci[6]++;
        if (tab[i] >= 11 )
            licznik_czestosci[7]++;
    }
    cout.width(10);
    for (int k=0 ; k<8;k++)

        cout << przedzialy[k] <<":"<<licznik_czestosci[k]<<endl;

}

void czestosc2(int tab[],int rozmiar,int *tablica)
{
//czestosc w poszczegolnych szeregach:
string szeregi[]={"(5,15>","(15,25>","(25,35>","(35,45>","(45,55>,","(55,65>"};
int szeregiInt[6]={0,0,0,0,0,0};
for (int i=0;i<rozmiar;i++)
{
    if (tab[i]>=5 && tab[i]<15)
        szeregiInt[0]++;
    if (tab[i]>=15 && tab[i]<25)
        szeregiInt[1]++;
    if (tab[i]>=25 && tab[i]<35)
        szeregiInt[2]++;
    if (tab[i]>=35 && tab[i]<45)
        szeregiInt[3]++;
    if (tab[i]>=45 && tab[i]<55)
        szeregiInt[4]++;
    if (tab[i]>=55 && tab[i]<=65)
        szeregiInt[5]++;

    for (int h=0;h<6;h++)
        tablica[h]=szeregiInt[h];
    }

// wysietlenie czestosci :

cout << "\nPRZEDZIAL:ILOSC\n";
for (int k=0;k<6;k++)
{
    cout << szeregi[k] << ":" << szeregiInt[k]<<endl;
}


}
int sredniaAr(int tab[],int rozmiar)
{
    int srodki[]={10,20,30,40,50,60};
    int suma ;
    for (int k=0;k<6;k++)
    {
        suma = suma + tab[k]*srodki[k];

    }
    cout << "\nSREDNIA AYTMETYCZNA TO:" << suma/rozmiar;
    return suma/rozmiar;

}
void mediana(int tab[],int rozmiar)

{
    int dolne[] = {5,15,25,35,45,55};
    int gorne[] = {15,25,35,45,55,65};

    int pozme = (rozmiar+1)/2;
    int liczebnosci[6];
    int licz=0;
    cout << "\nPOZYCJA MEDIANY:" << pozme << endl;
    cout << "\nLiczebnosci skumulowane:\n";
    for (int i=0;i<6;i++)
    {
       licz = licz + tab[i];
       liczebnosci[i] = licz;
       cout << i+1 << ":" << licz <<endl;
    }

    int przedzial;

    for (int k=0;k<6;k++)
    {
        if (pozme <= liczebnosci[k])
        {
            przedzial = k+1;
            break;
        }
    }
    cout << "MIESCI SIE W PRZEDZIALE NR " << przedzial << endl;
    cout << "WARTOSC MEDIANY TO:" << dolne[przedzial-1]+ ((pozme-liczebnosci[przedzial-1]) * (10/tab[przedzial-1]))  << endl;
}

void dominanta(int tab[])
{
    int pozdom = 0;
    int pozwar = 0;
    int dolne[] = {5,15,25,35,45,55};
    int gorne[] = {15,25,35,45,55,65};

    for (int i=0;i<6;i++)
    {
        if (tab[i] > pozwar)
        {
            pozwar=tab[i];
            pozdom = i;
        }
    }
    cout << "POZYCJA DOMINANTY:" << pozdom+1 << endl;
    double dominata = dolne[pozdom]+((tab[pozdom] - tab[pozdom-1]) / ((tab[pozdom] - tab[pozdom-1]) + (tab[pozdom] - tab[pozdom+1])))*10 ;

    cout << "WARTOSC:" << dominata << endl;
    //cout << "WARTOSC DOMINANTY:" << dolne[pozdom]; +  (((tab[pozdom] - tab[pozdom-1])/(tab[pozdom]-tab[pozdom-1]+tab[pozdom]+tab[pozdom+1]))*10);
}

void wariancja(int tab[],int rozmiar,int srednia)
{
    cout << "\n\nWARIANCJA:" << endl;
    float suma=0;
    int srodki[]={10,20,30,40,50,60};
    for (int i=0;i<6;i++)
    {
        suma = suma + ((srodki[i]-srednia)*(srodki[i]-srednia))*tab[i];
    }
    cout << "WARTOSC: " << suma/rozmiar << endl;
    cout << "ODCHYLENIE: " <<  sqrt(suma/rozmiar) << endl;
}

void rozstep (int tab[],int rozmiar)
{
    int mini=tab[0];
    int maxi=tab[0];

    for (int i=0;i<rozmiar;i++)
    {
        if (tab[i] < mini)
        {
            mini = tab[i];
        }
        if (tab[i] > maxi)
        {
            maxi = tab[i];
        }

    }
    cout << "ROZSTEP WYNOSI:" << maxi - mini<<endl;

}
void kwartyl(int tab[],int rozmiar)
{
sort (tab,tab+rozmiar);
for (int i=1;i<=3;i++)
{
    cout << "KWARTYL NR " << i << " JEST ROWNY " << tab[rozmiar*i/4] << endl;
}
}
