#include <iostream>
#include <chrono>
#include <random>
#include <algorithm>

using namespace std;
default_random_engine generator;
poisson_distribution<int> poisson(5); //
void rozklad_poissona_gen(int *tablica);
void szereg_rozdzielczy_punktowy(int *tablica,int a,int b);

class Dane_wejsciowe{
    protected:
        void rozklad_poissona_gen(int *tablica)
        {
            cout<<"Wygenerowanych 100 liczb wg rozkladu Poissona (Lambda = 5)\n";
            for (int i=0; i<100; ++i)
            {
                tablica[i]=poisson(generator);
                cout <<"[" << i+1 <<"]: " << tablica[i]<<endl;
            }
        }

        void szereg_rozdzielczy_punktowy(int *tablica,int minimum,int maximum)
        {
            cout<<"\nSzereg rozdzielczy punktowy\n";
            for (int i=minimum;i<=maximum;i++)
            {
                int ile_wystapien=0;
                for (int j=0;j<100;j++)
                {
                    if (tablica[j]==i)
                    ile_wystapien++;
                }
                cout<<"Liczba: "<<i<<" wystepuje: "<<ile_wystapien<<" razy.\n";
            }
}

};

class Dane_wyjsciowe: public Dane_wejsciowe{
    private:
        int rozklad_poissona[100];
        int *rozmiar_tablic;
    protected:
        Dane_wejsciowe dane_we;
        void zadanie_1a(int *tablica,int &rozmiar_tablicy)
        {
            rozmiar_tablicy=100;
            rozmiar_tablic=&rozmiar_tablicy;
            rozklad_poissona_gen(tablica);
            cout<<"\nMIN: "<<*min_element(tablica,tablica+rozmiar_tablicy);
            cout<<"\nMAX: "<<*max_element(rozklad_poissona,rozklad_poissona+rozmiar_tablicy);
            cout<<"\nRozstep: "<<*max_element(rozklad_poissona,rozklad_poissona+rozmiar_tablicy)-*min_element(rozklad_poissona,rozklad_poissona+rozmiar_tablicy);;
            sort(rozklad_poissona,rozklad_poissona+rozmiar_tablicy);
            szereg_rozdzielczy_punktowy(rozklad_poissona,*min_element(rozklad_poissona,rozklad_poissona+rozmiar_tablicy),*max_element(rozklad_poissona,rozklad_poissona+rozmiar_tablicy));
        }
    public:
        Dane_wyjsciowe();
};
Dane_wyjsciowe::Dane_wyjsciowe()
{
    zadanie_1a(rozklad_poissona,*rozmiar_tablic);
}

int main()
{
    Dane_wyjsciowe d_wy;


    return 0;
}
