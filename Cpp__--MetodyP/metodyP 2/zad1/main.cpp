#include <iostream>
#include <chrono>
#include <random>
#include <algorithm>
#include <conio.h>

using namespace std;
default_random_engine generator;
poisson_distribution<int> poisson(5); //lambda=5
normal_distribution<double> rozklad_normalny_gaussa(10,0.5); //srednia=10,odchylenie standardowe=0,5
void rozklad_poissona_gen(float *tablica);
void szereg_rozdzielczy_punktowy(float *tablica,int a,int b);

class Dane_wejsciowe{
    protected:
        void rozklad_poissona_gen(float *tablica)
        {
            cout<<"Wygenerowanych 100 liczb wg rozkladu Poissona (Lambda = 5)\n";
            for (int i=0;i<100;++i)
            {
                tablica[i]=poisson(generator);
                cout<<"Liczba: "<<i+1<<". wynosi: "<<tablica[i]<<"\n";
            }
        }
        void rozklad_normalny_gaussa_gen(float *tablica)
        {
            cout<<"Wygenerowanych 100 liczb wg rozkladu normalnego - Gaussa (Srednia = 10, Odchylenie stand. = 0,5)\n";
            for (int i=0;i<100;++i)
            {
                tablica[i]=rozklad_normalny_gaussa(generator);
                cout<<"Liczba: "<<i+1<<". wynosi: "<<tablica[i]<<"\n";
            }
        }
        void szereg_rozdzielczy_punktowy(float *tablica,int minimum,int maximum)
        {
            cout<<"\nSzereg rozdzielczy punktowy\n";
            for (int i=minimum;i<=maximum;i++)
            {
                int ile_wystapien=0;
                for (int j=0;j<100;j++)
                {
                    if (tablica[j]==i)
                    ile_wystapien++;
                }
                cout<<"Liczba: "<<i<<" wystepuje: "<<ile_wystapien<<" razy.\n";
            }
}
        void czestosc(float *tablica)
        {
            int suma1=0;
            int razem=0;
            float p1=9;
            float p2=9.5;
            int j=6;
            int tab[j];
            cout<<"\nSzereg rozdzielczy przedzialowy\n";
            cout<<"\nPodzial na 6 przedzialow [0 - 11,5]:";
            for (int i=0;i<100;i++)
            {
                if(tablica[i]<9)
                {
                    suma1+=1;
                }
            }
            cout<<"\n[0] - [9] wystepuje: "<<suma1<<" razy.";
            razem=suma1;
            for (int j=0;j<5;j++)
            {
                int suma2=0;
                for (int i=0;i<100;i++)
                {
                        if ((tablica[i]<(p2))-(tablica[i]<(p1)))
                        {
                            suma2+=1;
                        }
                }
                tab[j]=suma2;
                razem+=suma2;
                cout<<"\n["<<p1<<"] - ["<<p2<<"] wystepuje: "<<tab[j]<<" razy.";
                p1+=0.5;
                p2+=0.5;
            }
            cout<<"\nRazem: "<<razem<<"\n";
        }
        void czestosc(int *tablica,int rozmiar_tablicy3,int zaklad)
        {
           int suma1=0;
            int razem=0;
            int p1=5;
            int p2=15;
            int j=6;
            int tab[j];
            cout<<"\nPodzial na 6 przedzialow [5-65]:";
            for (int i=0;i<rozmiar_tablicy3;i++)
            {
                if(tablica[i]<15)
                {
                    suma1+=1;
                }
            }
            cout<<"\n5 - 15: "<<suma1;
            razem=suma1;
            for (int j=0;j<5;j++)
            {
                int suma2=0;
                for (int i=0;i<rozmiar_tablicy3;i++)
                {
                        if ((tablica[i]<(p2+10))-(tablica[i]<(p2)))
                        {
                            suma2+=1;
                        }
                }
                p1+=10;
                p2+=10;
                tab[j]=suma2;
                razem+=suma2;
                cout<<"\n"<<p1<<" - "<<p2<<": "<<tab[j];
            }
            cout<<"\nRazem: "<<razem<<"\n";
        }
        void szereg_skumulowany(float *tablica)
        {
            int razem=0;
            float p1=9;
            int j=6;
            int tab[j];
            cout<<"\nSzereg skumulowany\n";
            for (int j=0;j<6;j++)
            {
                int suma=0;
                for (int i=0;i<100;i++)
                {
                    if(tablica[i]<p1)
                    {
                        suma+=1;
                    }
                }
                tab[j]=suma;
                razem+=suma;
                cout<<"Liczb ponizej "<<p1<<" wystepuje: "<<tab[j]<<" razy.\n";
                p1+=0.5;
            }
        }
        void szereg_skumulowany(int *tablica,int rozmiar_tablicy3,int zaklad)
        {
            int razem=0;
            float p1=15;
            int j=6;
            int tab[j];
            cout<<"\nSzereg skumulowany\n";
            for (int j=0;j<6;j++)
            {
                int suma=0;
                for (int i=0;i<rozmiar_tablicy3;i++)
                {
                    if(tablica[i]<p1)
                    {
                        suma+=1;
                    }
                }
                tab[j]=suma;
                razem+=suma;
                cout<<"Liczb ponizej "<<p1<<" wystepuje: "<<tab[j]<<" razy.\n";
                p1+=10;
            }
        }
        float srednia_a(int *tablica,int rozmiar_tablicy3,int zaklad)
        {
            float wynik=0;
            for(int i=0;i<rozmiar_tablicy3;i++)
            {
                wynik+=tablica[i];
            }
            return wynik/rozmiar_tablicy3;
        }
        float srednia_h(int *tablica,int rozmiar_tablicy3,int zaklad)
        {
            float wynik=0;
            for(int i=0;i<rozmiar_tablicy3;i++)
            {
                wynik+=(1/(float)tablica[i]);
            }
            return rozmiar_tablicy3/wynik;
        }
        float wariancja (int *tablica,int rozmiar_tablicy3,int zaklad)
        {
            float wariancja=0;
            for (int i=0;i<rozmiar_tablicy3;i++)
            {
                wariancja+=pow((tablica[i]-srednia_a(tablica,rozmiar_tablicy3,zaklad)),2);//funkcja pow() sluzy do potegowania w tym wypadku do potegi 2
            }
        return wariancja/rozmiar_tablicy3;
        }
        float odch_stand (int *tablica,int rozmiar_tablicy3,int zaklad)
        {
            float odch_s;
            odch_s=sqrt(wariancja(tablica,rozmiar_tablicy3,zaklad));
            return odch_s;
        }
        float odch_sr (int *tablica,int rozmiar_tablicy3,int zaklad)
        {
            float odch_sre=0;
            for (int i=0;i<rozmiar_tablicy3;i++)
            {
                odch_sre+=abs((tablica[i]-srednia_a(tablica,rozmiar_tablicy3,zaklad)));//funkcja abs() oblicza wartosc bezwzgledna
            }
        return odch_sre/rozmiar_tablicy3;
        }
        void modalna(int *tablica,int rozmiar_tablicy3,int zaklad)
        {
            double ile = 0;
            double moda_kandydat,licznik,moda;
            for(int i=0;i<rozmiar_tablicy3;i++)
            {
                moda_kandydat = tablica[i];
                licznik = 0;
                for(int j=0;j<rozmiar_tablicy3;j++)
                    if(tablica[j] == moda_kandydat) licznik++;
                if(licznik>ile)
                {
                    ile=licznik;
                    moda=moda_kandydat;
                }
            }
        cout<<"\nModalna: "<<moda<<" wystepuje "<<ile<<" razy.";
        }
        float mediana(int *tablica,int rozmiar_tablicy3,int zaklad)
        {
            sort(tablica, tablica + rozmiar_tablicy3 );
            int median;
            if(rozmiar_tablicy3%2==0)
            median=(rozmiar_tablicy3/2)-1;
            else
            median=rozmiar_tablicy3/2;
            cout<<endl;
            return (float)tablica[median];
        }
};

class Dane_wyjsciowe: public Dane_wejsciowe{
    private:
        int tab_Zaklad1[55]={22, 26, 27, 27, 9,
                            11, 29, 33, 27, 28,
                            30, 31, 14, 33, 30,
                            16, 20, 18, 34, 33,
                            34, 30, 29, 24, 29,
                            29, 27, 42, 54, 60,
                            52, 36, 37, 43, 57,
                            39, 40, 36, 50, 44,
                            33, 31, 42, 49, 32,
                            36, 41, 32, 48, 31,
                            26, 36, 38, 37, 38};
        int tab_Zaklad2[65]={38, 37, 22, 31, 64,
                            39, 30, 26, 42, 44,
                            46, 41, 27, 34, 39,
                            16, 46, 60, 37, 36,
                            30, 26, 36, 51, 27,
                            32, 26, 20, 41, 27,
                            40, 44, 26, 19, 18,
                            54, 49, 28, 42, 43,
                            39, 38, 41, 50, 24,
                            33, 38, 36, 18, 53,
                            50, 59, 40, 36, 16,
                            36, 39, 48, 46, 58,
                            33, 43, 37, 44, 40};
        float rozklad_poissona[100];
        float rozklad_normalny_gauss[100];
        int *rozmiar_tablic;
    protected:
        Dane_wejsciowe dane_we;
        void zadanie_1a(float *tablica,int &rozmiar_tablicy)
        {
            rozmiar_tablicy=100;
            rozmiar_tablic=&rozmiar_tablicy;
            rozklad_poissona_gen(tablica);
            cout<<"\nMIN: "<<*min_element(tablica,tablica+*rozmiar_tablic);
            cout<<"\nMAX: "<<*max_element(rozklad_poissona,rozklad_poissona+*rozmiar_tablic);
            cout<<"\nRozstep: "<<*max_element(rozklad_poissona,rozklad_poissona+*rozmiar_tablic)-*min_element(rozklad_poissona,rozklad_poissona+*rozmiar_tablic);;
            sort(rozklad_poissona,rozklad_poissona+*rozmiar_tablic);
            cout<<"\n";
            szereg_rozdzielczy_punktowy(rozklad_poissona,*min_element(rozklad_poissona,rozklad_poissona+*rozmiar_tablic),*max_element(rozklad_poissona,rozklad_poissona+*rozmiar_tablic));
        }
        void zadanie_1b(float *tablica,int &rozmiar_tablicy2)
        {
            cout.precision(3);
            rozmiar_tablicy2=100;
            rozmiar_tablic=&rozmiar_tablicy2;
            rozklad_normalny_gaussa_gen(tablica);
            czestosc(rozklad_normalny_gauss);
            szereg_skumulowany(rozklad_normalny_gauss);
        }
        void zadanie_1c(int *tablica,int rozmiar_tablicy3,int zaklad)
        {
            cout<<"Zaklad "<<zaklad<<"\n";
            for (int i=0;i<rozmiar_tablicy3;i++)
            {
                if (i%5==0)
                {
                    cout<<"\n";
                }
                else
                cout<<tablica[i]<<" ";
            }
            cout<<"\n";
            czestosc(tablica,rozmiar_tablicy3,zaklad);
            szereg_skumulowany(tablica,rozmiar_tablicy3,zaklad);
            cout.precision(3);
            cout<<"\nDane statystyczne:\n";
            cout<<"Srednia arytmetyczna: "<<srednia_a(tablica,rozmiar_tablicy3,zaklad);
            cout<<"\nSrednia harmoniczna: "<<srednia_h(tablica,rozmiar_tablicy3,zaklad);
            cout<<"\nWariancja: "<<wariancja(tablica,rozmiar_tablicy3,zaklad);
            cout<<"\nOdchylenie standardowe: "<<odch_stand(tablica,rozmiar_tablicy3,zaklad);
            cout<<"\nOdchylenie srednie: "<<odch_sr(tablica,rozmiar_tablicy3,zaklad);
            modalna(tablica,rozmiar_tablicy3,zaklad);
            cout<<"Mediana: "<<mediana(tablica,rozmiar_tablicy3,zaklad);
        }
    public:
        Dane_wyjsciowe();

};
Dane_wyjsciowe::Dane_wyjsciowe()
{
    int wybor;
    do
    {
        cout<<"Wybierz zadanie:\n";
        cout<<"Zadanie 1 (a) :: Wcisnij 1.\n";
        cout<<"Zadanie 1 (b) :: Wcisnij 2.\n";
        cout<<"Zadanie 1 (c) :: Wcisnij 3.\n";
        cout<<"Wyjscie z programu :: Wcisnij 0.\n";
        cin>>wybor;
        switch (wybor)
        {
            case 1:
                zadanie_1a(rozklad_poissona,*rozmiar_tablic);
                break;
            case 2:
                zadanie_1b(rozklad_normalny_gauss,*rozmiar_tablic);
                break;
            case 3:
                zadanie_1c(tab_Zaklad1,55,1);
                cout<<"\n\n";
                zadanie_1c(tab_Zaklad2,65,2);
                break;
            default:
                exit(1);
        }
        getch();
        system("cls");
    }
    while (wybor!=0);

}

int main()
{
    Dane_wyjsciowe d_wy;
    return 0;
}
