#include <iostream>
#include <conio.h>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <cstdio>

using namespace std;

class Parametr_statystyczny {
    public:
        float srednia_a(int zaklad, int rozmiar, int *tablica)
        {
            float wynik=0;
            for(int i=0;i<rozmiar;i++)
            {
                wynik+=tablica[i];
            }
            return wynik/rozmiar;
        }
        float srednia_h(int zaklad, int rozmiar, int *tablica)
        {
            float wynik=0;
            for(int i=0;i<rozmiar;i++)
            {
                wynik+=(1/(float)tablica[i]);
            }
            return rozmiar/wynik;
        }
        float wariancja (int zaklad, int rozmiar, int *tablica)
        {
            float wariancja=0;
            for (int i=0;i<rozmiar;i++)
            {
                wariancja+=pow((tablica[i]-srednia_a(zaklad,rozmiar,tablica)),2);//funkcja pow() sluzy do potegowania w tym wypadku do potegi 2
            }
        return wariancja/rozmiar;
        }
        float odch_stand (int zaklad, int rozmiar, int *tablica)
        {
            float odch_s;
            odch_s=sqrt(wariancja(zaklad,rozmiar,tablica));
            return odch_s;
        }
        float odch_sr (int zaklad, int rozmiar, int *tablica)
        {
            float odch_sre=0;
            for (int i=0;i<rozmiar;i++)
            {
                odch_sre+=abs((tablica[i]-srednia_a(zaklad,rozmiar,tablica)));//funkcja abs() oblicza wartosc bezwzgledna
            }
        return odch_sre/rozmiar;
        }
        void max_min (int zaklad, int rozmiar, int *tablica)
        {
            int max=tablica[0];
            for (int i=0;i<rozmiar;i++)
            {
                if (tablica[i]>max) max=tablica[i];
            }

            int min=tablica[0];
            for (int i=0;i<=rozmiar-1;i++)
            {
                if (tablica[i]<min) min=tablica[i];
            }

            cout<<"\nMin: "<<min;
            cout<<"\nMax: "<<max;
        }
        void modalna(int zaklad, int rozmiar, int *tablica)
        {
            double ile = 0;
            double moda_kandydat,licznik,moda;
            for(int i=0;i<rozmiar;i++)
            {
                moda_kandydat = tablica[i];
                licznik = 0;
                for(int j=0;j<rozmiar;j++)
                    if(tablica[j] == moda_kandydat) licznik++;
                if(licznik>ile)
                {
                    ile=licznik;
                    moda=moda_kandydat;
                }
            }
        cout<<"\nModalna: "<<moda<<" wystepuje "<<ile<<" razy.\n";
    }
        float mediana(int zaklad, int rozmiar, int *tablica)
        {
            sort(tablica, tablica + rozmiar );
            int median;
            if(rozmiar%2==0)
            median=(rozmiar/2)-1;
            else
            median=rozmiar/2;
            cout<<endl;
            return (float)tablica[median];
        }
};

class Dane{
    private:
        int N;
        int M;
        int tab_Zaklad1[55]={22, 26, 27, 27, 9,
                            11, 29, 33, 27, 28,
                            30, 31, 14, 33, 30,
                            16, 20, 18, 34, 33,
                            34, 30, 29, 24, 29,
                            29, 27, 42, 54, 60,
                            52, 36, 37, 43, 57,
                            39, 40, 36, 50, 44,
                            33, 31, 42, 49, 32,
                            36, 41, 32, 48, 31,
                            26, 36, 38, 37, 38};
        int tab_Zaklad2[65]={38, 37, 22, 31, 64,
                            39, 30, 26, 42, 44,
                            46, 41, 27, 34, 39,
                            16, 46, 60, 37, 36,
                            30, 26, 36, 51, 27,
                            32, 26, 20, 41, 27,
                            40, 44, 26, 19, 18,
                            54, 49, 28, 42, 43,
                            39, 38, 41, 50, 24,
                            33, 38, 36, 18, 53,
                            50, 59, 40, 36, 16,
                            36, 39, 48, 46, 58,
                            33, 43, 37, 44, 40};
        Parametr_statystyczny par_stat;
    public:
        Dane();
    private:
        void przedzial(int zaklad, int rozmiar, int *tablica)
        {
            int suma1=0;
            int razem=0;
            int p1=5;
            int p2=15;
            int j=6;
            int tab[j];
            cout<<"\nPodzial na 6 przedzialow [5-65]:";

            for (int i=0;i<rozmiar;i++)
            {
                if(tablica[i]<15)
                {
                    suma1+=1;
                }
            }

            cout<<"\n5 - 15: "<<suma1;
            razem=suma1;

            for (int j=0;j<5;j++)
            {
                int suma2=0;

                for (int i=0;i<rozmiar;i++)
                {
                        if ((tablica[i]<(p2+10))-(tablica[i]<(p2)))
                        {
                            suma2+=1;
                        }
                }

                p1+=10;
                p2+=10;
                tab[j]=suma2;
                razem+=suma2;
                cout<<"\n"<<p1<<" - "<<p2<<": "<<tab[j];
            }
            cout<<"\nRazem: "<<razem<<"\n";
        }
        void dane_wyjsciowe(int zaklad, int rozmiar, int *tablica)
        {
            cout<<"Zaklad: "<<zaklad;
            for (int i=0;i<rozmiar;i++)
            {
                if (i%5==0)
                {
                    cout<<"\n";
                }
                else
                cout<<tablica[i]<<" ";
            }
            cout<<"\n";
            przedzial(zaklad,rozmiar,tablica);
            par_stat.max_min(zaklad,rozmiar,tablica);
            par_stat.modalna(zaklad,rozmiar,tablica);
            cout<<"Odchylenie srednie:: "<<par_stat.odch_sr(zaklad,rozmiar,tablica);
            cout<<"\nOdchylenie standardowe:: "<<par_stat.odch_stand(zaklad,rozmiar,tablica);
            cout<<"\nSrednia arytmetyczna:: "<<par_stat.srednia_a(zaklad,rozmiar,tablica);
            cout<<"\nSrednia harmoniczna:: "<<par_stat.srednia_h(zaklad,rozmiar,tablica);
            cout<<"\nWariancja:: "<<par_stat.wariancja(zaklad,rozmiar,tablica);
            cout<<"Mediana:: "<<par_stat.mediana(zaklad,rozmiar,tablica);
            cout<<"\n\n\n";
        }
};
Dane::Dane()
    : N( 55 )
    , M ( 65 )
{
    int swit_ch;
    cout.precision(3+1);
    do
    {
        cout<<"Wybierz Zaklad, ktorego dane chcesz wyswietlic:\n";
        cout<<"Zaklad I :: Wcisnij 1.\n";
        cout<<"Zaklad II :: Wcisnij 2.\n";
        cout<<"Oba Zaklady :: Wcisnij 3.\n";
        cout<<"Wyjscie z programu :: Wcisnij 0.\n";
        cin>>swit_ch;
        switch (swit_ch)
        {
            case 1:
                dane_wyjsciowe(1, N, tab_Zaklad1);
                break;
            case 2:
                dane_wyjsciowe(2, M, tab_Zaklad2);
                break;
            case 3:
                dane_wyjsciowe(1, N, tab_Zaklad1);
                dane_wyjsciowe(2, M, tab_Zaklad2);
                break;
            default:
                exit(1);
        }
        getch();
        system("cls");
    }
    while (swit_ch!=0);
}

int main()
{
    Dane wyswietl_parametry_statystyczne;
    return( 0 );
}
