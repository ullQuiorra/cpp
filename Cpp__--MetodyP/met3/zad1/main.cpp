#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <windows.h>
#define N 55
#define M 65

using namespace std;

class Dane {
    public:
        double tablica;
       void Dane_zakladu (int zaklad, int ilosc, double *tablica)
        {
            for (int i=0;i<ilosc;++i)
            {
                if (i%5!=0)
                cout<<tablica[i]<<" ";
                else
                {
                cout<<"\n";
                cout<<tablica[i]<<" ";
                }
            }
        }
        void przedzial(int zaklad, int ilosc, double *tablica)
        {
            int suma1=0;
            int razem=0;
            int p1=5;
            int p2=15;
            int j=6;
            int tab[j];
            cout<<"\n\nPodzial na 6 przedzialow [5-65]:";

            for (int i=0;i<ilosc;i++)
            {
                if(tablica[i]<15)
                {
                    suma1+=1;
                }
            }

            cout<<"\n5 - 15: "<<suma1;
            razem=suma1;

            for (j=0;j<5;j++)
            {
                int suma2=0;

                for (int i=0;i<ilosc;i++)
                {
                        if ((tablica[i]<(p2+10))-(tablica[i]<(p2)))
                        {
                            suma2+=1;
                        }
                }

                p1+=10;
                p2+=10;
                tab[j]=suma2;
                razem+=suma2;
                cout<<"\n"<<p1<<" - "<<p2<<": "<<tab[j];
            }
            cout<<"\nRazem: "<<razem;
        }
        void sortowanie(int zaklad, int ilosc, double *tablica) //Funkcja sortowanie wymagana jest do wyznaczenia mediany
        {
            cout<<"\nPosortowana tablica babelkowo: ";
            for (int i=0;i<ilosc-1;i++)
            {
                for (int j=0;j<ilosc-1;j++)
                {
                    if (tablica[j]>tablica[j+1])
                        swap(tablica[j],tablica[j+1]);
                }
            }
            for (int i=0;i<ilosc;i++)
            {
                cout<<tablica[i]<<", ";
            }
        }

    Dane()
    {
         double tab_Zaklad1[N]={22, 26, 27, 27, 9,
                            11, 29, 33, 27, 28,
                            30, 31, 14, 33, 30,
                            16, 20, 18, 34, 33,
                            34, 30, 29, 24, 29,
                            29, 27, 42, 54, 60,
                            52, 36, 37, 43, 57,
                            39, 40, 36, 50, 44,
                            33, 31, 42, 49, 32,
                            36, 41, 32, 48, 31,
                            26, 36, 38, 37, 38};
         double tab_Zaklad2[M]={38, 37, 22, 31, 64,
                            39, 30, 26, 42, 44,
                            46, 41, 27, 34, 39,
                            16, 46, 60, 37, 36,
                            30, 26, 36, 51, 27,
                            32, 26, 20, 41, 27,
                            40, 44, 26, 19, 18,
                            54, 49, 28, 42, 43,
                            39, 38, 41, 50, 24,
                            33, 38, 36, 18, 53,
                            50, 59, 40, 36, 16,
                            36, 39, 48, 46, 58,
                            33, 43, 37, 44, 40};
    }
};

class Parametr_statystyczny {
public:
    float srednia_a(int zaklad, int ilosc, double *tablica)
    {
        float wynik=0;
        for(int i=0;i<ilosc;i++)
        {
            wynik+=tablica[i];
        }
        return wynik/ilosc;
    }
    double srednia_h(int zaklad, int ilosc, double *tablica)
    {
        double wynik=0;
        for(int i=0;i<ilosc;i++)
        {
            wynik+=(1/tablica[i]);
        }
        return ilosc/wynik;
    }
    float wariancja (int zaklad, int ilosc, double *tablica)
    {
        double wariancja=0;
        for (int i=0;i<ilosc;i++)
        {
            wariancja+=pow((tablica[i]-srednia_a(zaklad,ilosc,tablica)),2);//funkcja pow() sluzy do potegowania w tym wypadku do potegi 2
        }
    return wariancja/ilosc;
    }
    float odch_stand (int zaklad, int ilosc, double *tablica)
    {
        float odch_s;
        odch_s=sqrt(wariancja(zaklad,ilosc,tablica));
        return odch_s;
    }
    float odch_sr (int zaklad, int ilosc, double *tablica)
    {
        float odch_sre=0;
        for (int i=0;i<ilosc;i++)
        {
            odch_sre+=abs((tablica[i]-srednia_a(zaklad,ilosc,tablica)));//funkcja abs() oblicza wartosc bezwzgledna
        }
    return odch_sre/ilosc;
    }
    void max_min (int zaklad, int ilosc, double *tablica)
    {
        int max=tablica[0];
        for (int i=0;i<ilosc;i++)
        {
            if (tablica[i]>max) max=tablica[i];
        }

        int min=tablica[0];
        for (int i=0;i<=ilosc-1;i++)
        {
            if (tablica[i]<min) min=tablica[i];
        }

        cout<<"\nMin: "<<min;
        cout<<"\nMax: "<<max;
    }
    void modalna(int zaklad, int ilosc, double *tablica)
    {
        double ile = 0;
        double moda_kandydat,licznik,moda;
        for(int i=0;i<ilosc;i++)
        {
            moda_kandydat = tablica[i];
            licznik = 0;
            for(int j=0;j<ilosc;j++)
                if(tablica[j] == moda_kandydat) licznik++;
            if(licznik>ile)
            {
                ile=licznik;
                moda=moda_kandydat;
            }
        }
        cout<<"\nModalna: "<<moda<<" wystepuje "<<ile<<" razy.\n";
    }
    int mediana(int zaklad, int ilosc, double *tablica)
    {
        int median;
        if(ilosc%2==0)
        median=(ilosc/2)-1;
        else
        median=ilosc/2;
        cout<<endl;
        return tablica[median];
    }
};

class Gotowe_dane {
public:
    Dane dan_e;
    Parametr_statystyczny par_stat;
    void wypisz_dane (int zaklad, int ilosc, double *tablica)
    {
        if (zaklad==1)
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE | FOREGROUND_INTENSITY);
        }
        else
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_INTENSITY);
        }
        cout.precision(3+1);
        cout<<"Zaklad "<<zaklad;
        dan_e.Dane_zakladu(zaklad, ilosc, tablica);
        dan_e.przedzial(zaklad, ilosc, tablica);
        cout<<"\n\nSrednia arytmetyczna: "<<par_stat.srednia_a(zaklad, ilosc, tablica);
        cout<<"\nSrednia harmoniczna: "<<par_stat.srednia_h(zaklad, ilosc, tablica);
        cout<<"\nWariancja: "<<par_stat.wariancja(zaklad, ilosc, tablica);
        cout<<"\nOdchylenie standardowe: "<<par_stat.odch_stand(zaklad, ilosc, tablica);
        cout<<"\nOdchylenie srednie: "<<par_stat.odch_sr(zaklad, ilosc, tablica);
        par_stat.max_min(zaklad, ilosc, tablica);
        par_stat.modalna(zaklad, ilosc, tablica);
        dan_e.sortowanie(zaklad, ilosc, tablica);
        cout<<"Mediana: "<<par_stat.mediana(zaklad, ilosc, tablica);
        cout<<"\n\n-----------------------------------------------------------------\n\n";
    }
    void wyswietl_dane ()
    {
        wypisz_dane(1, N, dan_e.tab_Zaklad1);
        wypisz_dane(2, M, dan_e.tab_Zaklad2);
    }



};

int main()
{
    Gotowe_dane wypisz_dane;
    wypisz_dane.wyswietl_dane();
    return 0;
}
