#include <iostream>
#include <sstream>
#include <windows.h>

///////////////////////////////////////
////Title: Karabin/////////////////////
////Author: Mateusz Mucha//////////////
////Description: Projekt na zajecia////
///////////////////////////////////////

using namespace std;
enum ErrorType {
    Critical , Fatal , Warning
};

class Error {
      int errorTypeSelect;
  public:
      Error(ErrorType errorTypeSelect) {
          this->errorTypeSelect = errorTypeSelect;
      }
      virtual string getErrorReason() = 0;
      virtual int getErrorCode() = 0;
      virtual string getSeparatorToErrorCode() {
          return "##";
      }
      virtual string getErrorType() {
          stringstream stream;
          switch ( errorTypeSelect ) {
            case Critical:
                stream << "Critical ERROR 0x";
                break;
            case Fatal:
                stream << "Fatal 0x";
                break;
            case Warning:
                stream << "Warning 0x";
                break;
            default:
                throw "Unknown ErrorType!";
        }
        return stream.str();
      }
      virtual string getErrorMessage() {
          stringstream stream;
          stream << getSeparatorToErrorCode() << getErrorType() << std::hex<<getErrorCode() << getSeparatorToErrorCode()
                 << "\n" << getErrorReason() << "\n\n";
          return stream.str();
      }
};
class ErrorWrongAmmoType : public Error {
  public:
      ErrorWrongAmmoType() : Error(Critical) {}
      int getErrorCode() {
          return 18;
      }
      string getErrorReason() {
          return "You can choose just one of four type of bullets. Your choose doesn't exist.";
      }
};
class ErrorInvalidCharacterType : public Error {
  public:
      ErrorInvalidCharacterType() : Error(Fatal) {}
      int getErrorCode() {
          return 28;
      }
      string getErrorReason() {
          return "You used invalid characters.";
      }
};
class ErrorInvalidBulletsNumber : public Error {
  public:
      ErrorInvalidBulletsNumber() : Error(Warning) {}
      int getErrorCode() {
          return 38;
      }
      string getErrorReason() {
          return "You cannot have empty of magazine or more than 30 bullets!\nCheck your choose, maybe you write wrong characters.";
      }
};

class Bullet {
  public:
    virtual Bullet * clone() const = 0;
    virtual float damagePerShot() = 0;
    virtual string animationOfShot() = 0;
    virtual ~Bullet() {}
};
class Explosive : public Bullet {
  protected:
    virtual Explosive * clone() const {
        return new Explosive( *this );
    }
    string animationOfShot() {
        return "BooM !!!\n";
    }
    float damagePerShot() {
        return 7.4;
    }
  public:
      virtual ~Explosive() {}
};
class Incendiary : public Bullet {
  protected:
    virtual Incendiary * clone() const {
        return new Incendiary( *this );
    }
    string animationOfShot() {
        return "Fire Fire Fire !!!\n";
    }
    float damagePerShot() {
        return 4.1;
    }
  public:
      virtual ~Incendiary() {}
};
class Streaked : public Bullet {
  protected:
    virtual Streaked * clone() const {
        return new Streaked( *this );
    }
    string animationOfShot() {
        return "Tssssssss !!!\n";
    }
    float damagePerShot() {
        return 1.7;
    }
  public:
      virtual ~Streaked() {}
};
class Blank : public Bullet {
  protected:
    virtual Blank * clone() const {
        return new Blank( *this );
    }
    string animationOfShot() {
        return "Pudlo !!!\n";
    }
    float damagePerShot() {
        return 0.0;
    }
  public:
      virtual ~Blank() {}
};

class Magazine {
    Bullet *bullets[ 30 ]; // max 30 Bullets
    int newBulletIndex;
    int bulletType = 0;
  public:
    Magazine() {
        for ( int i = 0 ; i < 30 ; i++ ) {
            bullets[ i ] = NULL;
        }
        newBulletIndex = 0;
    }
    void chooseBulletType() {
        cout << "Wybierz rodzaj " << newBulletIndex + 1 << " pocisku\n1 - Odlamkowy\n2 - Zapalajacy\n3 - Smugowy\n4 - Slepak\n";
        cin>>bulletType;
    }
    void addBulletsToMagazine() {
        do {
            chooseBulletType();
            if ( bulletType > 4 )
                throw ErrorWrongAmmoType();
            if ( bulletType == 1 ) {
                bullets[ newBulletIndex ] = new Explosive;
                newBulletIndex++;
            }
            if ( bulletType == 2 ) {
                bullets[ newBulletIndex ]= new Incendiary;
                newBulletIndex++;
            }
            if ( bulletType == 3 ) {
                bullets[ newBulletIndex ] = new Streaked;
                newBulletIndex++;
            }
            if ( bulletType == 4 ) {
                bullets[ newBulletIndex ] = new Blank;
                newBulletIndex++;
            }
        } while ( bulletType > 4 );
        if ( bulletType <= 0 )
            throw ErrorInvalidCharacterType();
    }
    float damageOfShots() {
        float damageOfShot = 0.0;
        for ( int i = 0 ; i < newBulletIndex ; i++ )
        {
            damageOfShot += bullets[ i ] -> damagePerShot();
        }
        return damageOfShot;
    }
    Magazine( Magazine & copyMagazine ) {
        for ( int i = 0 ; i < 30 ; i++ ) {
            delete bullets[ i ];
            bullets[ i ] = NULL;
        }
        for ( int i = 0 ; i < newBulletIndex ; i++ ) {
            bullets[ i ] = ( * ( copyMagazine.bullets[ i ] ) ).clone();
        }
    };
    Magazine & operator = ( Magazine & copyMagazine )
    {
        if ( this != & copyMagazine ) {
            newBulletIndex = copyMagazine.newBulletIndex;
            for ( int i = 0 ; i < newBulletIndex ; i++ ) {
                bullets[ i ]=( * ( copyMagazine.bullets[ i ] ) ).clone();
            }
        }
        return *this;
    };
    virtual Magazine * clone() const {
        return new Magazine;
    }
    virtual ~Magazine() {
        for ( int i = 0 ; i < newBulletIndex ; i++ ) {
            delete bullets[ i ];
        }
        newBulletIndex = 0;
    }
};
class Gun {
    Magazine * full_magazine;
    int howBullets;
  public:
    Gun( int bulletsHowMuch ) {
        if ( ( bulletsHowMuch > 30 ) or ( bulletsHowMuch <= 0 ) )
            throw ErrorInvalidBulletsNumber();
        howBullets = bulletsHowMuch;
        full_magazine = new Magazine;
    }
    void createGun() {
        for ( int i = 0 ; i < howBullets ; i++ ) {
            ( *full_magazine ).addBulletsToMagazine();
            system ( "cls" );
        }
    }
    float totalDamageOfGun() {
        return ( *full_magazine ).damageOfShots();
    }
    Gun( Gun & copyGun ) {
        full_magazine = ( * ( copyGun.full_magazine ) ).clone();
        *full_magazine = ( * ( copyGun.full_magazine ) );
    }
    Gun & operator = ( Gun & copyGun ) {
        if ( this != & copyGun ) {
            delete full_magazine;
            full_magazine = ( * ( copyGun.full_magazine ) ).clone();
        }
        return *this;
    };
    virtual ~Gun() {
        delete full_magazine;
        full_magazine = NULL;
    }
};

ostream & operator << ( ostream & stream , Gun & gun ) {
    stream << gun.totalDamageOfGun();
    return stream;
}

int main() {
    int ile_naboi;
    cout << "Ile chcesz wystrzelic naboi?\n";
    cin>>ile_naboi;
    system ( "cls" );
    try {
        Gun M4( ile_naboi );
        M4.createGun();
        system ( "cls" );
        cout << "== Wyniki Twoich broni ==\n";
        cout << "\nSuma zadanych obrazen dla M4: " << M4;
        Gun M3 = M4;
        cout << "\nSuma zadanych obrazen dla M3: " << M3;
        Gun M2 = M3;
        cout << "\nSuma zadanych obrazen dla M2: " << M2;
    }
    catch ( Error & errorTypeMessage ) {
        system ( "cls" );
        system ( "color 04" );
        cout << errorTypeMessage.getErrorMessage();
        return errorTypeMessage.getErrorCode();
    }
}
