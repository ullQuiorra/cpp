#include <iostream>
#include <cstdlib>
#include <conio.h>

using namespace std;

class Amunicja{
    public:
        virtual void wystrzel()
        {
            cout<<"Pif-Paf!\n";
        }
};

class Odlamkowe : public Amunicja{
    public:
        void wystrzel()
        {
            cout<<"BooM !!!!\n";
        }
};

class Zapalajace : public Amunicja{
    public:
        void wystrzel()
        {
            cout<<"fire fire fire !!!\n";
        }
};

class Smugowe : public Amunicja{
    public:
        void wystrzel()
        {
            cout<<"Tssssss !!!\n";
        }
};

class Magazynek{
    protected:
        int pojemnosc_magazynku;
        Amunicja *amunicja_ammo[3];
        float obrazenia[3]={6.7,3.5,1.7};
    public:
        Magazynek(int ilosc_naboi, int rodzaj_amunicji)
        {
            pojemnosc_magazynku=ilosc_naboi;
                if (rodzaj_amunicji==0)
                {
                    amunicja_ammo[0]=new Odlamkowe;
                    cout<<"Utworzyles magazynek z liczba naboi = "<<pojemnosc_magazynku<<" o obrazeniach: "<<obrazenia[rodzaj_amunicji]<<" kazdy.\n";
                    (*amunicja_ammo[0]).wystrzel();
                }
                else
                if (rodzaj_amunicji==1)
                {
                    amunicja_ammo[1]=new Zapalajace;
                    cout<<"Utworzyles magazynek z liczba naboi = "<<pojemnosc_magazynku<<" o obrazeniach: "<<obrazenia[rodzaj_amunicji]<<" kazdy.\n";
                    (*amunicja_ammo[1]).wystrzel();
                }
                else
                if (rodzaj_amunicji==2)
                {
                    amunicja_ammo[2]=new Smugowe;
                    cout<<"Utworzyles magazynek z liczba naboi = "<<pojemnosc_magazynku<<" o obrazeniach: "<<obrazenia[rodzaj_amunicji]<<" kazdy.\n";
                    (*amunicja_ammo[2]).wystrzel();
                }
        }
};

class Karabin{
    protected:
        Magazynek *magazynek;
    public:
        Karabin(int ilosc_naboi, int rodzaj_ammo)
        {
            magazynek=new Magazynek(ilosc_naboi,rodzaj_ammo);
        }
        void utworz_bron(int ilosc_naboi, int rodzaj_ammo)
        {
            cout<<"\nZadales: '"<<"' obrazen!";
        }
};

int main()
{
    Karabin *karabinek;
    int ile_naboi,rodzaj_ammo;
    cout<<"Podaj ile chcesz miec naboi: ";
    cin>>ile_naboi;
    cout<<"Wybierz rodzaj amunicji: \n\n0 - Odlamkowe\n1 - Zapalajace\n2 - Smugowe\n";
    cin>>rodzaj_ammo;
    karabinek=new Karabin(ile_naboi,rodzaj_ammo);
    (*karabinek).utworz_bron(ile_naboi,rodzaj_ammo);
    delete karabinek;

    return 0;
}
