#include <iostream>
#include <cstdlib>

using namespace std;

class Pocisk{
    public:
        virtual float dmg()=0;
        virtual void animacja_strzalu()
        {
            cout<<"Pif - Paf !!!";
        }
    protected:
        virtual ~Pocisk(){};
};

class Odlamkowy: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"BooM !!!\n";
        }
        float dmg()
        {
            return 7.4;
        }
};

class Zapalajacy: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"Fire Fire Fire !!!\n";
        }
        float dmg()
        {
            return 4.1;
        }
};

class Smugowy: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"Tssssssss !!!\n";
        }
        float dmg()
        {
            return 1.7;
        }
};

class Magazynek{
        Pocisk *ammo_dmg;
        Pocisk *amunicja[30];
        float razem_dmg=0;
    public:
        void zapelnij_magazynek(int rodzaj_pocisku)
        {
            if (rodzaj_pocisku==1)
            {
                for(int i=0;i<30;i++)
                {
                    amunicja[i]=new Odlamkowy;
                }
            }
            if (rodzaj_pocisku==2)
            {
                for(int i=0;i<30;i++)
                {
                    amunicja[i]=new Zapalajacy;
                }
            }
            if (rodzaj_pocisku==3)
            {
                for(int i=0;i<30;i++)
                {
                    amunicja[i]=new Smugowy;
                }
            }
        }
        void odglos_broni()
        {
            for (int i=0;i<30;i++)
            {
                if (NULL!=amunicja[i])
                {
                    cout<<i+1<<". ";
                    amunicja[i]->animacja_strzalu();
                }
            }
        }
        float suma_obrazen_pelnego_magazynka()
        {
            for (int i=0;i<30;i++)
            {
                if (NULL!=amunicja[i])
                {
                    razem_dmg+=amunicja[i]->dmg();
                }
            }
            return razem_dmg;
        }
//Funkcje dla wybranych przez uzytkownika pociskow
        void zapelnij_magazynek(int rodzaj_pocisku1,int rodzaj_pocisku2,int rodzaj_pocisku3)
        {
            int suma_naboi=rodzaj_pocisku1+rodzaj_pocisku2+rodzaj_pocisku3;
            for (int i=0;i<suma_naboi;i++)
            {
                for (int j=0;j<rodzaj_pocisku1;j++)
                {
                    amunicja[i]=new Odlamkowy;
                    i++;
                }
                for (int k=0;k<rodzaj_pocisku2;k++)
                {
                    amunicja[i]=new Zapalajacy;
                    i++;
                }
                for (int l=0;l<rodzaj_pocisku3;l++)
                {
                    amunicja[i]=new Smugowy;
                    i++;
                }
            }

        }
        void odglos_broni(int rodzaj_pocisku1,int rodzaj_pocisku2,int rodzaj_pocisku3)
        {
            int suma_naboi=rodzaj_pocisku1+rodzaj_pocisku2+rodzaj_pocisku3;
            for (int i=0;i<suma_naboi;i++)
            {
                if (NULL!=amunicja[i])
                {
                    cout<<i+1<<". ";
                    amunicja[i]->animacja_strzalu();
                }
            }
        }
        float suma_obrazen_pelnego_magazynka(int rodzaj_pocisku1,int rodzaj_pocisku2,int rodzaj_pocisku3)
        {
            int suma_naboi=rodzaj_pocisku1+rodzaj_pocisku2+rodzaj_pocisku3;
            for (int i=0;i<suma_naboi;i++)
            {
                for(int j=0;j<rodzaj_pocisku1;j++)
                {
                    if (NULL!=amunicja[i])
                    {
                        razem_dmg+=amunicja[i]->dmg();
                    }
                    i++;
                }
                for(int k=0;k<rodzaj_pocisku2;k++)
                {
                    if (NULL!=amunicja[i])
                    {
                        razem_dmg+=amunicja[i]->dmg();
                    }
                    i++;
                }
                for(int l=0;l<rodzaj_pocisku3;l++)
                {
                    if (NULL!=amunicja[i])
                    {
                        razem_dmg+=amunicja[i]->dmg();
                    }
                    i++;
                }
            }
            return razem_dmg;
        }
};

class Karabin{
        int typ_ammo=0;
        int typ_ammo1=0;
        int typ_ammo2=0;
        int typ_ammo3=0;
        Magazynek zawartosc_magazynka;
    public:
        void utworz_bron()
        {
            cout<<"Wybierz typ amunicji\n1 - Odlamkowy\n2 - Zapalajacy\n3 - Smugowy\n";
            cin>>typ_ammo;
            system("cls");
            zawartosc_magazynka.zapelnij_magazynek(typ_ammo);
            zawartosc_magazynka.odglos_broni();
            cout<<"\nZadales "<<zawartosc_magazynka.suma_obrazen_pelnego_magazynka()<<" obrazen!\n";
        }
        void wybierz_opcje()
        {
            cout<<"Podaj, ile chcesz naboi odlamkowych: ";
            cin>>typ_ammo1;
            cout<<"Podaj, ile chcesz naboi zapalajacych: ";
            cin>>typ_ammo2;
            cout<<"Podaj, ile chcesz naboi smugowych: ";
            cin>>typ_ammo3;
            zawartosc_magazynka.zapelnij_magazynek(typ_ammo1,typ_ammo2,typ_ammo3);
            zawartosc_magazynka.odglos_broni(typ_ammo1,typ_ammo2,typ_ammo3);
            cout<<"\nZadales "<<zawartosc_magazynka.suma_obrazen_pelnego_magazynka(typ_ammo1,typ_ammo2,typ_ammo3)<<" obrazen!\n";
        }
};

int main()
{
    int wybor_opcji;
    Karabin M4;
    do{
    cout<<"Wybierz opcje\n0 - Wyjscie\n1 - Pelny magazynek identycznych naboi [30 naboi w magazynku]\n2 - Wybierz jakie chcesz naboje\n";
    cin>>wybor_opcji;
    switch (wybor_opcji)
    {
        case 0:
            exit(1);
        case 1:
            M4.utworz_bron();
            break;
        case 2:
            M4.wybierz_opcje();
            break;
    }
    system("pause");
    system("cls");
    }while(wybor_opcji!=0);
    return 0;
}
