#include <iostream>

using namespace std;

class Q {
int x;
public:
  Q(): x(10) {
    cout <<"Q():x(10)="<<x<<'\n';
    x=20;
    cout <<"Q::x="<<x<<'\n';
  }
  void display() {
    cout << "x="<<x<<'\n';
  }
};

int main() {
  Q obiekt;
  obiekt.display();
}
