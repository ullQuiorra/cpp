#include <iostream>

using namespace std;

class Q {
  int *x;
public:
 Q(int y): x(new int(y)) {
    if (y>=10) {
       cout <<"y powinien byc mniejszy od 10! Ustawiam y na 2!";
       y=2;
    }
 }
 void display() {
    cout <<"x="<<*x;
 }
 virtual ~Q() {delete x;}
};
int main() {
  int a=142;
  Q obj(a);
  obj.display();
}
