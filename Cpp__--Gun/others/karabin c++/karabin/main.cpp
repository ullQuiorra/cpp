#include <iostream>
#include <cstdlib>
#include <conio.h>

using namespace std;

class Amunicja{
    protected:
        int pojemnosc_magazynka=0;
        float trafien_razem=0;
        float dmg=0.0;
        float *damage;
        Amunicja()
        {
            damage=&dmg;
        }
    public:
        virtual void wystrzel()
        {
            cout<<"\nPif-Paf!\n\n";
        }
        void wystrzel(int ile_razy)
        {
            if (ile_razy<=pojemnosc_magazynka)
            {
                pojemnosc_magazynka=ile_razy;
            }
            else
            {
                cout<<"Chcesz wystrzelic wiecej naboi niz masz w magazynku!\nPodaj nowa wartosc [max "<<pojemnosc_magazynka<<"]:";
                cin>>ile_razy;
                pojemnosc_magazynka=ile_razy;
            }
            trafien_razem=pojemnosc_magazynka*(*damage);
            cout<<"Zadales: "<<trafien_razem<<" obrazen.";
            cout<<"\n\n";
        }

};

class Odlamkowe : public Amunicja{
    public:
        void wystrzel()
        {
           cout<<"\nBooM !!!\n";
        }
        Odlamkowe()
        {
            *damage=8.2;
            pojemnosc_magazynka=30;
        }

};

class Zapalajace : public Amunicja{
    public:
        void wystrzel()
        {
            cout<<"\nfirefirefire!\n";
        }
        Zapalajace()
        {
            *damage=5.4;
            pojemnosc_magazynka=20;
        }
};

class Smugowe : public Amunicja{
    public:
        void wystrzel()
        {
            cout<<"\nTsssssssssss!\n";
        }
        Smugowe()
        {
            *damage=1.7;
            pojemnosc_magazynka=10;
        }
};

class Karabin : public Amunicja{
        Amunicja *ammo;
    public:
        void ammo_odlamkowe(string rodzaj_broni, string nazwa_broni)
        {
            cout<<"Utworzyles bron "<<nazwa_broni<<" z amunicja odlamkowa.\n";
            ammo=new Odlamkowe();
            (*ammo).wystrzel();
            cout<<"\nWybierz ile naboi chcesz wystrzelic: ";
            cin>>pojemnosc_magazynka;
            (*ammo).wystrzel(pojemnosc_magazynka);
        }
        void ammo_zapalajace(string rodzaj_broni, string nazwa_broni)
        {
            cout<<"Utworzyles bron "<<nazwa_broni<<" z amunicja zapalajaca.\n";
            ammo=new Zapalajace();
            (*ammo).wystrzel();
            cout<<"\nWybierz ile naboi chcesz wystrzelic: ";
            cin>>pojemnosc_magazynka;
            (*ammo).wystrzel(pojemnosc_magazynka);
        }
        void ammo_smugowe(string rodzaj_broni, string nazwa_broni)
        {
            cout<<"Utworzyles bron "<<nazwa_broni<<" z amunicja smugowa.\n";
            ammo=new Smugowe();
            (*ammo).wystrzel();
            cout<<"\nWybierz ile naboi chcesz wystrzelic: ";
            cin>>pojemnosc_magazynka;
            (*ammo).wystrzel(pojemnosc_magazynka);
        }
};

int main()
{
    int wybor_broni;
    int wybor_amunicji;
    Karabin m4;
    Karabin snajperka;
    do{
        cout<<"Jaki typ broni chcesz utworzyc: \n";
        cout<<"1 - Snajperka\n";
        cout<<"2 - M4\n";
        cout<<"0 - Wyjscie\n";
        cin>>wybor_broni;
        switch (wybor_broni)
        {
            case 0:
                exit(1);
            case 1:
                cout<<"\nWybierz rodzaj amunicji dla twojej nowejbroni:\n";
                cout<<"1 - Odlamkowe \n";
                cout<<"2 - Zapalajace \n";
                cout<<"3 - Smugowe \n";
                cout<<"0 - Wyjscie \n";
                cin>>wybor_amunicji;
                switch (wybor_amunicji)
                {
                    case 0:
                        exit(1);
                    case 1:
                        snajperka.ammo_odlamkowe("Odlamkowe","Snajperka");
                        break;
                    case 2:
                        snajperka.ammo_zapalajace("Zapalajace","Snajperka");
                        break;
                    case 3:
                        snajperka.ammo_smugowe("Smugowe","Snajperka");
                        break;
                }
                getch();
                system("cls");
                break;
            case 2:
                cout<<"Wybierz rodzaj amunicji dla twojej nowej broni:\n";
                cout<<"1 - Odlamkowe \n";
                cout<<"2 - Zapalajace \n";
                cout<<"3 - Smugowe \n";
                cout<<"0 - Wyjscie \n";
                cin>>wybor_amunicji;
                switch (wybor_amunicji)
                {
                    case 0:
                        exit(1);
                    case 1:
                        m4.ammo_odlamkowe("Odlamkowe","M4");
                        break;
                    case 2:
                        m4.ammo_zapalajace("Zapalajace","M4");
                        break;
                    case 3:
                        m4.ammo_smugowe("Smugowe","M4");
                        break;
                }
                getch();
                system("cls");
                break;
    }
    }while(wybor_broni!=0);
    return 0;
}
