#include <iostream>
#include <cstdlib>
#include <numeric>

using namespace std;

class Pocisk{
    public:
        virtual float dmg()=0;
        virtual void animacja_strzalu()=0;
        virtual ~Pocisk(){}
};

class Odlamkowy: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"BooM !!!\n";
        }
        float dmg()
        {
            return 7.4;
        }
    public:
        virtual ~Odlamkowy(){}
};

class Zapalajacy: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"Fire Fire Fire !!!\n";
        }
        float dmg()
        {
            return 4.1;
        }
    public:
        virtual ~Zapalajacy(){}
};

class Smugowy: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"Tssssssss !!!\n";
        }
        float dmg()
        {
            return 1.7;
        }
    public:
        ~Smugowy(){}
};

class Slepak: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"Pudlo !!!\n";
        }
        float dmg()
        {
            return 0.0;
        }
    public:
        virtual ~Slepak(){}
};

class Magazynek{
        Pocisk *amunicja[30];
        int nowy_pocisk,rodzaj_pocisku;
        float razem_dmg;
    public:
        Magazynek()
        {
            for (int i=0;i<30;i++)
            {
                amunicja[i]=NULL;
            }
            nowy_pocisk=0;
            rodzaj_pocisku=0;
            razem_dmg=0.0;
        }
        void dodaj_pocisk_do_magazynka()
        {
            do
            {
                cout<<"\n| Wybierz Typ "<<nowy_pocisk+1<<" pocisku |\n1 - Odlamkowy\n2 - Zapalajacy\n3 - Smygowy\n4 - Slepak\n";
                cin>>rodzaj_pocisku;
                if (rodzaj_pocisku==1)
                {
                    amunicja[nowy_pocisk]=new Odlamkowy;
                    nowy_pocisk++;
                }
                if (rodzaj_pocisku==2)
                {
                    amunicja[nowy_pocisk]=new Zapalajacy;
                    nowy_pocisk++;
                }
                if (rodzaj_pocisku==3)
                {
                    amunicja[nowy_pocisk]=new Smugowy;
                    nowy_pocisk++;
                }
                if (rodzaj_pocisku==4)
                {
                    amunicja[nowy_pocisk]=new Slepak;
                    nowy_pocisk++;
                }
            }while(rodzaj_pocisku>4);
        }
        void odglos_broni()
        {
            for (int i=0;i<nowy_pocisk;i++)
            {
                cout<<i+1<<". ";
                amunicja[i]->animacja_strzalu();
            }
        }
        float suma_obrazen_pelnego_magazynka()
        {
            for (int i=0;i<nowy_pocisk;i++)
            {
                razem_dmg+=amunicja[i]->dmg();
            }
            return razem_dmg;
        }
        /*Magazynek(Magazynek &wzor_magazynek)
        {
            for (int i=0;i<30;i++)
            {
                *amunicja[i]=*(wzor_magazynek.amunicja[i]);
            }
        }*/
        virtual ~Magazynek()
        {
            for(int i=0;i<nowy_pocisk;i++)
            {
                delete amunicja[i];
            }
        }
};

class Karabin{
        Magazynek *nowy_magazynek;
        int ilosc_naboi,typ_pocisku;
    public:
        Karabin(int ile_pociskow)
        {
            nowy_magazynek=new Magazynek;
            while(ile_pociskow>30)
            {
                cout<<"Nie masz tyle miejsca w magazynku! Mozesz MAX 30!\n";
                cin>>ile_pociskow;
            }
            ilosc_naboi=ile_pociskow;
            typ_pocisku=0;
        }
        void utworz_bron()
        {
            for(int i=0;i<ilosc_naboi;i++)
            {
                (*nowy_magazynek).dodaj_pocisk_do_magazynka();
            }
        }
        float suma_zadanych_obrazen()
        {
            return (*nowy_magazynek).suma_obrazen_pelnego_magazynka();
        }
        /*Karabin(Karabin &wzor_karabin)
        {
            cout<<"\nKonstruktor kopiuajacy";
            nowy_magazynek=new Magazynek;
            *nowy_magazynek=*(wzor_karabin.nowy_magazynek);
        }*/
        Karabin& operator=(Karabin &wzor_karabinu)
        {
            if(this!=&wzor_karabinu)
            {
                delete nowy_magazynek;
                nowy_magazynek=new Magazynek;
                *nowy_magazynek=*(wzor_karabinu.nowy_magazynek);
            }
            return *this;
        }
        virtual ~Karabin()
        {
            delete nowy_magazynek;
        }
};

ostream& operator<<(ostream &wypisz_suma,Karabin &karabin)
{
    return wypisz_suma<<karabin.suma_zadanych_obrazen();
}

int main()
{
    int wybor_opcji,ile_naboi;
    do{
    cout<<"Wybierz opcje\n0 - Wyjscie\n1 - Utworz bron\n";
    cin>>wybor_opcji;
    switch (wybor_opcji)
    {
        case 0:
            {
                exit(1);
            }
        case 1:
            {
                cout<<"\nIle chcesz miec pociskow [MAX 30] ! ";
                cin>>ile_naboi;
                Karabin M4(ile_naboi);
                M4.utworz_bron();
                cout<<"\nZadales "<<M4<<" obrazen.\n\n";
                /*Karabin M3=M4;
                cout<<"\nZadales "<<M3.suma_zadanych_obrazen()<<" obrazen.\n\n";*/
            }
            break;
    }
    system("pause");
    system("cls");
    }while(wybor_opcji!=0);
    return 0;
}
