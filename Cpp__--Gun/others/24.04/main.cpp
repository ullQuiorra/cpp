#include <iostream>
#include <cstdlib>
#include <numeric>

using namespace std;

class Pocisk{
    public:
        virtual float dmg()=0;
        virtual void animacja_strzalu()
        {
            cout<<"Pif - Paf !!!";
        }
    protected:
        virtual ~Pocisk(){};
};

class Odlamkowy: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"BooM !!!\n";
        }
        float dmg()
        {
            return 7.4;
        }
};

class Zapalajacy: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"Fire Fire Fire !!!\n";
        }
        float dmg()
        {
            return 4.1;
        }
};

class Smugowy: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"Tssssssss !!!\n";
        }
        float dmg()
        {
            return 1.7;
        }
};

class Slepak: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"Pudlo !!!\n";
        }
        float dmg()
        {
            return 0.0;
        }
};

class Magazynek{
        Pocisk *amunicja[30];
        int nowy_pocisk=0;
        float razem_dmg=0;
    public:
        Magazynek()
        {
            for (int i=0;i<30;i++)
            {
                amunicja[i]=NULL;
            }
        }
        void zapelnij_magazynek(int rodzaj_pocisku)
        {
            if (rodzaj_pocisku==1)
            {
                    amunicja[nowy_pocisk]=new Odlamkowy;
                    nowy_pocisk++;
            }
            if (rodzaj_pocisku==2)
            {
                    amunicja[nowy_pocisk]=new Zapalajacy;
                    nowy_pocisk++;
            }
            if (rodzaj_pocisku==3)
            {
                    amunicja[nowy_pocisk]=new Smugowy;
                    nowy_pocisk++;
            }
            if (rodzaj_pocisku==4)
            {
                    amunicja[nowy_pocisk]=new Slepak;
                    nowy_pocisk++;
            }
        }
        void odglos_broni()
        {
            for (int i=0;i<30;i++)
            {
                if (NULL!=amunicja[i])
                {
                    cout<<i+1<<". ";
                    amunicja[i]->animacja_strzalu();
                }
            }
        }
        float suma_obrazen_pelnego_magazynka(int ile_naboi)
        {
            for (int i=0;i<ile_naboi;i++)
            {
                if (NULL!=amunicja[i])
                {
                    razem_dmg+=amunicja[i]->dmg();
                }
            }
            return razem_dmg;
        }
};

class Karabin{
        Magazynek zawartosc_magazynka;
        int ile_naboi;
    public:
        Karabin(int ile_pociskow)
        {
            if(ile_pociskow<=30)
            {
                ile_naboi=ile_pociskow;
            }
            else
            {
                while(ile_pociskow>30)
                {
                    cout<<"Nie masz tyle miejsca w magazynku! Mozesz MAX 30!\n";
                    cin>>ile_pociskow;
                }
                ile_naboi=ile_pociskow;
            }
        }
        void utworz_bron()
        {
            int typ_ammo=0;
            for (int i=0;i<ile_naboi;i++)
            {
                cout<<"\nWybierz rodzaj "<<i+1<<" pocisku\n1 - Odlamkowy\n2 - Zapalajacy\n3 - Smugowy\n4 - Slepak\n";
                cin>>typ_ammo;
                zawartosc_magazynka.zapelnij_magazynek(typ_ammo);
            }
        }
        void wydaj_dzwiek()
        {
            zawartosc_magazynka.odglos_broni();
        }
        void suma_zadanych_obrazen()
        {
            cout<<"\nZadales: "<<zawartosc_magazynka.suma_obrazen_pelnego_magazynka(ile_naboi)<<" obrazen!\n\n";
        }
        ~Karabin(){};
};

int main()
{
    int wybor_opcji,ile_naboi;
    do{
    cout<<"Wybierz opcje\n0 - Wyjscie\n1 - Utworz bron\n";
    cin>>wybor_opcji;
    switch (wybor_opcji)
    {
        case 0:
            exit(1);
        case 1:
            cout<<"\nIle chcesz miec pociskow? [MAX 30] ";
            cin>>ile_naboi;
            Karabin M4(ile_naboi);
            M4.utworz_bron();
            M4.suma_zadanych_obrazen();
            M4.~Karabin();
            break;
    }
    system("pause");
    system("cls");
    }while(wybor_opcji!=0);
    return 0;
}
