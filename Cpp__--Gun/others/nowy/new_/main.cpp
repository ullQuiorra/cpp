#include <iostream>
#include <cstdlib>
#include <numeric>

using namespace std;

class Error{
      string errorType="none";
  public:
      Error()
      {
          cout<<"\n\nError detected!";
      }
};
class Error_1 : public Error{
      string errorType="0x01";
  public:
      Error_1()
      {
          cout<<"\n\n== Fatal ERROR "<<errorType<<" ==\nYou can choose just one of four type of bullets. Your choose doesn't exist.\n";
          exit(1);
      }
};
class Error_2 : public Error{
      string errorType="0x02";
  public:
      Error_2()
      {
          cout<<"\n\n== Fatal ERROR "<<errorType<<"==\nYou used invalid characters.\n";
          exit(1);
      }
};
class Error_3 : public Error{
      string errorType="0x03";
  public:
      Error_3()
      {
          cout<<"\n== Fatal ERROR "<<errorType<<"==\nYou cannot have empty of magazine or more than 30 bullets!\nCheck your choose, maybe you write wrong characters.\n";
          exit(1);
      }
};

class Bullet{
  public:
    virtual Bullet* clone() const=0;
    virtual float damagePerShot()=0;
    virtual void animationOfShot()=0;
    virtual ~Bullet(){}
};
class Explosive: public Bullet{
  protected:
    virtual Explosive* clone() const
    {
        return new Explosive(*this);
    }
    void animationOfShot()
    {
        cout<<"BooM !!!\n";
    }
    float damagePerShot()
    {
        return 7.4;
    }
  public:
    virtual ~Explosive(){}
};
class Incendiary: public Bullet{
  protected:
    virtual Incendiary* clone() const
    {
        return new Incendiary(*this);
    }
    void animationOfShot()
    {
        cout<<"Fire Fire Fire !!!\n";
    }
    float damagePerShot()
    {
        return 4.1;
    }
  public:
    virtual ~Incendiary(){}
};
class Streaked: public Bullet{
  protected:
    virtual Streaked* clone()  const
    {
        return new Streaked(*this);
    }
    void animationOfShot()
    {
        cout<<"Tssssssss !!!\n";
    }
    float damagePerShot()
    {
        return 1.7;
    }
  public:
    ~Streaked(){}
};
class Blank: public Bullet{
  protected:
    virtual Blank* clone() const
    {
        return new Blank(*this);
    }
    void animationOfShot()
    {
        cout<<"Pudlo !!!\n";
    }
    float damagePerShot()
    {
        return 0.0;
    }
  public:
    virtual ~Blank(){}
};

class Magazine{
    Bullet *bullets[30]; // max 30 Bullets
    int newBulletIndex;
  public:
    Magazine()
    {
        for(int i=0;i<30;i++)
        {
            bullets[i] = NULL;
        }
        newBulletIndex=0;
    }
    void addBulletsToMagazine()
    {
        int bulletType=0;
        do{
            cout<<"\nWybierz rodzaj "<<newBulletIndex+1<<" pocisku\n1 - Odlamkowy\n2 - Zapalajacy\n3 - Smugowy\n4 - Slepak\n";
            cin>>bulletType;
            if(bulletType>4)
                throw Error_1();
            if (bulletType==1)
            {
                bullets[newBulletIndex]=new Explosive;
                newBulletIndex++;
            }
            if (bulletType==2)
            {
                bullets[newBulletIndex]=new Incendiary;
                newBulletIndex++;
            }
            if (bulletType==3)
            {
                bullets[newBulletIndex]=new Streaked;
                newBulletIndex++;
            }
            if (bulletType==4)
            {
                bullets[newBulletIndex]=new Blank;
                newBulletIndex++;
            }
        }while(bulletType>4);
        if(bulletType<=0)
            throw Error_2();
    }
    float damageOfShots()
    {
        float damageOfShot=0.0;
        for(int i=0;i<newBulletIndex;i++)
        {
            damageOfShot+=bullets[i]->damagePerShot();
        }
        return damageOfShot;
    }
    Magazine(Magazine &copyMagazine)
    {
        cout<<"\nKONSTRUKTOR KOPIUAJACY Magazynka\n";
        for (int i=0;i<30;i++)
        {
            delete bullets[i];
            bullets[i]=NULL;
        }
        for(int i=0;i<newBulletIndex;i++)
        {
            bullets[i]=(*(copyMagazine.bullets[i])).clone();
            *bullets[i]=*(copyMagazine.bullets[i]);
        }
    };
    Magazine& operator=(Magazine &copyMagazine)
    {
        if(this!=&copyMagazine)
        {
            cout<<"\n::Operator przypisania Magazynka::";
            newBulletIndex=copyMagazine.newBulletIndex;
            for(int i=0;i<newBulletIndex;i++)
            {
                delete bullets[i];
                bullets[i]==NULL;
            }
            for(int i=0;i<newBulletIndex;i++)
            {
                bullets[i]=(*(copyMagazine.bullets[i])).clone();
                *bullets[i]=*(copyMagazine.bullets[i]);
            }
        }
        return *this;
    };
    virtual Magazine* clone() const
    {
        return new Magazine;
    }
    virtual ~Magazine()
    {
        for(int i=0;i<newBulletIndex;i++)
        {
            delete bullets[i];
        }
        newBulletIndex=0;
    }
};
class Gun{
    Magazine *full_magazine;
    int howBullets;
  public:
    Gun(int bulletsHowMuch)
    {
        if((bulletsHowMuch>30)or(bulletsHowMuch<=0))
            throw Error_3();
        howBullets=bulletsHowMuch;
        full_magazine=new Magazine;
    }
    void createGun()
    {
        for(int i=0;i<howBullets;i++)
        {
            (*full_magazine).addBulletsToMagazine();
        }
    }
    float totalDamageOfGun()
    {
        return (*full_magazine).damageOfShots();
    }
    Gun(Gun &copyGun)
    {
        cout<<"\n\nKONSTRUKTOR KOPIUJACY Karabinu";
        full_magazine=(*(copyGun.full_magazine)).clone();
        *full_magazine=(*(copyGun.full_magazine));
    }
    Gun& operator=(Gun &copyGun)
    {
        if(this!=&copyGun)
        {
            cout<<"\n\n::Operator przypisania Karabinu::";
            delete full_magazine;
            full_magazine=(*(copyGun.full_magazine)).clone();
            *full_magazine=*(copyGun.full_magazine);
        }
        return *this;
    };
    virtual ~Gun()
    {
        delete full_magazine;
        full_magazine=NULL;
    }
};

ostream& operator<<(ostream &stream,Gun &gun)
{
    stream<<gun.totalDamageOfGun();
    return stream;
}

int main()
{
    int ile_naboi;
    cout<<"Ile chcesz wystrzelic naboi?\n";
    cin>>ile_naboi;
    Gun M4(ile_naboi);
    M4.createGun();
    cout<<"\nSuma zadanych obrazen dla M4: "<<M4;
    Gun M3=M4;
    cout<<"\nSuma zadanych obrazen dla M3: "<<M3<<"\n";
    Gun M2=M3;
    cout<<"\nSuma zadanych obrazen dla M2: "<<M2<<"\n";
}
