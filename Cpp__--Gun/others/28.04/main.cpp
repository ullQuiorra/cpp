#include <iostream>
#include <cstdlib>
#include <numeric>

using namespace std;

class Pocisk{
    public:
        virtual float dmg()=0;
        virtual void animacja_strzalu()=0;
        virtual ~Pocisk(){}
};

class Odlamkowy: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"BooM !!!\n";
        }
        float dmg()
        {
            return 7.4;
        }
    public:
        ~Odlamkowy(){}
};

class Zapalajacy: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"Fire Fire Fire !!!\n";
        }
        float dmg()
        {
            return 4.1;
        }
    public:
        ~Zapalajacy(){}
};

class Smugowy: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"Tssssssss !!!\n";
        }
        float dmg()
        {
            return 1.7;
        }
    public:
        ~Smugowy(){}
};

class Slepak: public Pocisk{
    protected:
        void animacja_strzalu()
        {
            cout<<"Pudlo !!!\n";
        }
        float dmg()
        {
            return 0.0;
        }
    public:
        ~Slepak(){}
};

class Magazynek{
        Pocisk *amunicja[30];
        int nowy_pocisk=0;
        float razem_dmg=0;
    public:
        Magazynek()
        {
            for (int i=0;i<30;i++)
            {
                amunicja[i]=NULL;
            }
        }
        void zapelnij_magazynek(int rodzaj_pocisku)
        {
            if (rodzaj_pocisku==1)
            {
                    amunicja[nowy_pocisk]=new Odlamkowy;
                    nowy_pocisk++;
            }
            if (rodzaj_pocisku==2)
            {
                    amunicja[nowy_pocisk]=new Zapalajacy;
                    nowy_pocisk++;
            }
            if (rodzaj_pocisku==3)
            {
                    amunicja[nowy_pocisk]=new Smugowy;
                    nowy_pocisk++;
            }
            if (rodzaj_pocisku==4)
            {
                    amunicja[nowy_pocisk]=new Slepak;
                    nowy_pocisk++;
            }
        }
        void odglos_broni()
        {
            for (int i=0;i<30;i++)
            {
                if (NULL!=amunicja[i])
                {
                    cout<<i+1<<". ";
                    amunicja[i]->animacja_strzalu();
                }
            }
        }
        float suma_obrazen_pelnego_magazynka(int ile_naboi)
        {
            for (int i=0;i<ile_naboi;i++)
            {
                if (NULL!=amunicja[i])
                {
                    razem_dmg+=amunicja[i]->dmg();
                }
            }
            return razem_dmg;
        }
        /*Magazynek(Magazynek &wzor_magazynek)
        {
            for (int i=0;i<30;i++)
            {
                amunicja[i]=wzor_magazynek.amunicja[i];
            }
        }*/
        virtual ~Magazynek()
        {
            int naboje_do_usuniecia=0;
            for (int i=0;i<30;i++)
            {
                if(amunicja[i]!=NULL)
                {
                    delete amunicja[i];
                    naboje_do_usuniecia++;
                }
            }
            cout<<"Usunieto "<<naboje_do_usuniecia<<" lusek po nabojach\n";
        }
};

class Karabin{
        int typ_pocisku;
        int *ilosc_naboi;
        Magazynek *nowy_magazynek=new Magazynek;
    public:
        Karabin(int ile_pociskow):typ_pocisku(0)
        {
            ilosc_naboi=new int;
            while(ile_pociskow>30)
            {
                cout<<"Nie masz tyle miejsca w magazynku! Mozesz MAX 30!\n";
                cin>>ile_pociskow;
            }
            (*ilosc_naboi)=ile_pociskow;
        }
        void utworz_bron()
        {
            for (int i=0;i<(*ilosc_naboi);i++)
            {
                do
                {
                    cout<<"\nWybierz rodzaj "<<i+1<<" pocisku\n1 - Odlamkowy\n2 - Zapalajacy\n3 - Smugowy\n4 - Slepak\n";
                    cin>>typ_pocisku;
                }
                while(typ_pocisku>4);
                (*nowy_magazynek).zapelnij_magazynek(typ_pocisku);
            }
        }
        float suma_zadanych_obrazen()
        {
            return (*nowy_magazynek).suma_obrazen_pelnego_magazynka( (*ilosc_naboi) );
        }
        /*Karabin(Karabin &wzor_karabin)
        {
            nowy_magazynek=new Magazynek;
            *nowy_magazynek=*(wzor_karabin.nowy_magazynek);
        }*/
        virtual ~Karabin()
        {
            delete ilosc_naboi;
            delete nowy_magazynek;
        }
};

int main()
{
    int wybor_opcji,ile_naboi;
    do{
    cout<<"Wybierz opcje\n0 - Wyjscie\n1 - Utworz bron\n";
    cin>>wybor_opcji;
    switch (wybor_opcji)
    {
        case 0:
            {
                exit(1);
            }
        case 1:
            {
                cout<<"\nIle chcesz miec pociskow [MAX 30] ! ";
                cin>>ile_naboi;
                Karabin M4(ile_naboi);
                M4.utworz_bron();
                cout<<"\nZadales "<<M4.suma_zadanych_obrazen()<<" obrazen.\n\n";
                /*Karabin M3(M4);
                cout<<"\nZadales "<<M4.suma_zadanych_obrazen()<<" obrazen.\n\n";*/
            }
            break;
    }
    system("pause");
    system("cls");
    }while(wybor_opcji!=0);
    return 0;
}
