#include <iostream>
#include <cstdlib>
#include <conio.h>

using namespace std;

class Amunicja{
    protected:
        float dmg;
        Amunicja(int damage)
        {
            dmg=damage;
        }
    public:
        virtual void wystrzel()
        {
            cout<<"Pif-Paf!\n";
        }
        virtual float damage()
        {
            return dmg;
        }
};

class Odlamkowe : public Amunicja{
    public:
        void wystrzel()
        {
            cout<<"BooM !!!!\n";
        }
};

class Zapalajace : public Amunicja{
    public:
        void wystrzel()
        {
            cout<<"fire fire fire !!!\n";
        }
};

class Smugowe : public Amunicja{
    public:
        void wystrzel()
        {
            cout<<"Tssssss !!!\n";
        }
};

class Magazynek{
    protected:
        int pojemnosc_magazynku;
    public:
        Magazynek(int ilosc_naboi)
        {
            pojemnosc_magazynku=ilosc_naboi;
            cout<<"Utworzyles magazynek z liczba naboi = "<<pojemnosc_magazynku<<"\n";
        }
};

class Karabin{
    protected:
        Magazynek *magazynek;
    public:
        Karabin(int ilosc_naboi)
        {
            magazynek=new Magazynek(ilosc_naboi);
        }
        void utworz_bron(int rodzaj_ammo)
        {
            cout<<"Utworzyles bron!\n";
        }
};

int main()
{
    Karabin *karabin;
    int naboje;
    cout<<"Podaj liczbe naboi w magazynku: ";
    cin>>naboje;
    karabin=new Karabin(naboje);
    delete karabin;
    cout<<"\n\n\n";
    return 0;
}
