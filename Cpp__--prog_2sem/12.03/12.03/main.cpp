#include <iostream>

using namespace std;

int main()
{
    int a=32;
    float b=4.4;

    int *wsk_i=&a;
    float *wsk_f=&b ;
// & - operator wy�uskiwania adresu zmiennej
// warto�ci� zmiennej wsk_i  jest adres zmiennej a
// warto�ci� zmiennej wsk_f  jest adres zmiennej b
    cout<<*wsk_f<<endl;
// wypisze warto�� zmiennej b
// tzn. wypisze warto�� zmiennej, kt�rej adres jest
// warto�ci� zmiennej wska�nikowej wsk_f
// * operator wy�uskania warto�ci zmiennej, na kt�r�
// pokazuje wska�nik wsk_f
    cout<<*wsk_i<<endl;    // wypisze warto�� zmiennej a
    cout<<wsk_i<<endl;     //  wypisze adres zmiennej a
    cout<<wsk_f<<endl;     // wypisze adres zmiennej b

    *wsk_i = 344; 		// zmiana warto�ci zmiennej a = 344
    *wsk_f = 4.5;
    *wsk_i =*wsk_f;              // zmiana warto�ci zmiennej a = 4 (trywialna konwersja float-> int

    void *wsk_v;
    wsk_v=wsk_i;
//cout <<*wsk_v; // niepoprawne
    cout <<*(float*)wsk_v; // warto�� nieokre�lona
    cout <<*(int*)wsk_v; // wypisze 34 czyli warto�� zmiennej a
//wsk_i=wsk_v;  // b��d
    wsk_i=(int*)wsk_v; // poprawne


    return 0;
}
