#include <iostream>

using namespace std;

void wsk_1();
void wsk_2();
void wsk_3();
void wsk_4();

int main()
{


    cout<<"\n\n\n\n";
    return 0;
}

void wsk_1()
{
    int *w;
    int k=3;
    w=&k;
    cout<<"Od tej pory *w to to samo co k\n";
    cout<<"Operator '&' jest WYLUSKANIEM adresu zmiennej dlatego w="<<w<<" w tym wypadku jest to adres ziennej k";
    cout<<"\nObiekt 'w' o adresie: "<<w<<" pokazuje wartosc:"<<(*w)<<" poniewaz '*' wskazuje nam obiekt pokazywany przez wskaznik dzieki '&'";
    cout<<"\nZwyczajnie zmienna k="<<k;
    cout<<"\n&k jest adresem: "<<&k;
    cout<<"\n&w jest adresem: "<<&w;
    // cout<<"\n*k jest: "<<*k; // nie mozna, bo nie zadeklarowano wskaznika *k
}

void wsk_2()
{
    int x=8;
    int y=4;
    int *wskaznik;
    wskaznik=&x; // od teraz wskaznik pokazuje nam na adres zmiennej x
    cout<<"Zmienna= "<<x<<" a odczytana przez wskaznik= "<<*wskaznik<<"\n"; //skoro wskaznik pokazuje na zmienna x, to zmienna x=*wskaznik
    x=10; //zmiana wartosci zmiennej x
    cout<<"Zmienna= "<<x<<" a odczytana przez wskaznik= "<<*wskaznik<<"\n"; //skoro zmienilismy wartosc zmiennej x, to odteraz ta wartosc bedzie wskazywana. wskaznik zauwazyl zmiane zmiennej na ktora pokazuje.
    *wskaznik=200; //do tego na co pokazuje wskaznik (*wskaznik) przypisz wartosc 200 czyli do zmiennej x wpisujemy teraz 200
    cout<<"Zmienna= "<<x<<" a odczytana przez wskaznik= "<<*wskaznik<<"\n"; //tak jak wyzej, ukaze nam sie 200
    wskaznik=&y; //teraz dla wskaznika przypisujemy adres zmiennej y wiec odteraz wskaznik nie pokazuje juz na X tylko na Y
    cout<<"Zmienna= "<<x<<" a odczytana przez wskaznik= "<<*wskaznik; //najpierw wypisze 200 poniewaz wyzej przypisalismy 200 do zmiennej X, jednak (*wskaznik) teraz pokaze nam nowa wartosc zmiennej Y

}

void wsk_3()
{
    int liczba=9;
    int *wsk_liczba;
    wsk_liczba=&liczba;
    cout<<"Tyle: "<<wsk_liczba<<"\n"; // pokazuje adres zmiennej liczbe :: wsk_liczba=&liczba
    cout<<"Tyle: "<<*wsk_liczba<<"\n"; // pokazuje wartosc spod adresu wskaznika :: *wskaznik => *(wsk_liczba=&liczba)=liczba=9
    cout<<"Tyle: "<<&wsk_liczba<<"\n"; // pokazuje adres wskaznika
}

void wsk_4()
{
    int x;
    int a;
    int **c;
    int *b;
    int ***e;
    int ***d;
    a=2;
    x=4;
    b=&a;
    c=&b;
    e=&c;
    d=e;
    cout<<"x: "<<x<<"\n"; //pokazuje wartiosc x
    cout<<"&a: "<<&a<<"\n"; // pokazuje adres zmiennej a
    cout<<"a: "<<a<<"\n"; // pokazuje wartosc zmiennej a
    cout<<"*b: "<<*b<<"\n"; //pokazuje wartosc zmiennej b czyli adres zmiennej a :: *b=*(b=&a) =>wskaznik na adres a => a zmienna a=2
    cout<<"b: "<<b<<"\n"; // adres zmiennej a
    cout<<"c: "<<c<<"\n"; // adres zmiennej b
    cout<<"*c: "<<*c<<"\n"; //adres zmiennej a :: *c=*(c=&b) =>b pokazuje adres adres zmiennej a wiec *c=*(&b) a wskaznik na adres wynosi b=&a
    cout<<"**c: "<<**c<<"\n"; // wartosc zmiennej a :: tak samo jak wyzej => pierwszy wskaznik pokazuje na adres zmiennej a na ktory jest kolejny wsklaznik *(&a)=2 => **c=*(*(c=&b)) => **c=*(b=&a) => **c=*(&a) => *cc=2
    cout<<"e: "<<e<<"\n"; //adres zmiennej c
    cout<<"*e: "<<*e<<"\n"; // adres b :: *e=*(e=&c) => *e=*(&c=&b) => *e=&b
    cout<<"d: "<<d<<"\n"; // adres zmiennej c
    cout<<"*d: "<<*d<<"\n"; // adres zmiennej b :: *d=*(d=e=&c) => *d=*(&c) => *d=c=&b
    cout<<"**d: "<<**d<<"\n"; // tak jak wyzej :: **d=*(*(c=&b)) => **d=*(b=&a) => **d=&a
    cout<<"***d: "<<***d<<"\n"; // adres zmiennej a :: tak jak wyzej => ***d=*(&a) => **d=&a=2
}
