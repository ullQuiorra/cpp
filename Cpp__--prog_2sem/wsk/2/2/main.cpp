#include <iostream>

using namespace std;

void zwieksz_o2(int *);
void przeladuj(int *);
void dodawanie (int *, int *);
void odejmowanie (int *, int *);

int main()
{
    int liczba;
    int *wskaznik=&liczba;
    int liczba2;
    int *wskaznik2=&liczba2;
    cout<<"Tyle: "<<*wskaznik<<"\n"; // losowa wartosc
    liczba=4;
    cout<<"Tyle: "<<*wskaznik<<"\n"; // wartosc 4
    liczba=8;
    cout<<"Tyle: "<<*wskaznik<<"\n"; // wartosc 8

    zwieksz_o2(&liczba);
    przeladuj(&liczba);
    dodawanie(&liczba,&liczba2);
    odejmowanie(&liczba,&liczba2);

    return 0;
}

void zwieksz_o2(int *liczba)
{
    *liczba+=2;
    cout<<"Liczbateraz wynosi wg funkcji: "<<*liczba;
}
void przeladuj(int *liczba)
{
    cout<<"\nLiczba teraz wynosi: "<<*liczba; // wynosi 10 bo zapisala sie wartosc z funkcji zwieksz_o2
    *liczba=3; // nowa wartosc dlawskaznika doliczba
    cout<<"\nA teraz: "<<*liczba; // teraz wyswietli sie nowa wartosc =3
}
void dodawanie (int *liczba, int *liczba2)
{
    *liczba=17;
    *liczba2=20;
    cout<<"\nSuma liczbawynosi: "<<*liczba+*liczba2;
}
void odejmowanie (int *liczba, int *liczba2)
{
    *liczba2=7;
    cout<<"\nSuma liczbawynosi: "<<*liczba-*liczba2; // wyisze 10 bo wskaznik na liczba=17 - nowy wskaznik =7
}
