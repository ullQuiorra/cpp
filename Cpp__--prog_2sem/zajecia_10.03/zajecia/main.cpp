#include <iostream>

using namespace std;

class D{
public:
    int x;
    int y;
};

class C{
public:
    int x;
    D a;
};

class B{
public:
    int y;
    C a;
};

class A{
public:
    int x;
    B a;
};

int main()
{
    A a;
    a.x=(a.a.y+a.a.a.x-a.a.a.a.x)*a.a.a.a.y;
    return 0;
}
