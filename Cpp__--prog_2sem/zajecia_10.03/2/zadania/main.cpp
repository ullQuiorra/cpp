#include <iostream>

using namespace std;

class Suma{
public:
    int x;
    int y;

    int zwroc_suma()
    {
        return x+y;
    }

    void przypisz()
    {
        cout<<"Podaj pierwsza liczbe: \n";
        cin>>x;
        cout<<"Podaj druga liczbe: \n";
        cin>>y;
    }
};

int main()
{
    Suma f_suma;
    f_suma.przypisz();
    cout<<"\n";
    cout<<f_suma.zwroc_suma();

    return 0;
}
