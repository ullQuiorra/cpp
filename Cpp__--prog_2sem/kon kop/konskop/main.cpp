#include <iostream>

using namespace std;

class A{
    public:
        void f()
        {
            cout<<"Zwykla funkcja - A\n";
        }
        virtual void vf()
        {
            cout<<"Wirtualna funkcja - vA\n";
        }
};

class B : public A{
    public:
        void f()
        {
            cout<<"Zwykla funkcja - B\n";
        }
        virtual void vf()
        {
            cout<<"Wirtualna funkcja - vB\n";
        }
};

int main()
{
    /*A a;
    B b;

    a.f();
    a.vf();
    b.f();
    b.vf();
    cout<<"\n\n";*/

    /*A *wskA;
    wskA=new A;
    (*wskA).f();
    (*wskA).vf();
    cout<<"\n\n";*/

    /*A *wskA;
    wskA=new B;
    (*wskA).f();
    (*wskA).vf();*/

    /*B *wskB;
    wskB=new B;
    (*wskB).f();
    (*wskB).vf();*/

    /*B *wskB;
    wskB=new A;
    (*wskB).f();
    (*wskB).vf();*/

    return 0;
}
