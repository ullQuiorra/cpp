#include <iostream>

using namespace std;

void zad1 (int);
void zad2 ();
int f_cja1(int *, int *);

int main()
{
    //int liczba=7;
    //int *wsk_liczba;
    //wsk_liczba=&liczba;
    //zad1(*wsk_liczba);

    //zad2();

    int liczba1=7;
    int liczba2=2;
    cout<<"Liczba 1 -main : "<<liczba1<<"\n";
    cout<<"Liczba 2 -main : "<<liczba2<<"\n";
    f_cja1(&liczba1,&liczba2);
    cout<<"Liczba 1 -main : "<<liczba1<<"\n";
    cout<<"Liczba 2 -main : "<<liczba2<<"\n";

    cout<<"\n\n\n";
    return 0;
}

void zad1 (int liczba)
{
    liczba+=2;
    cout<<"Zmienna: "<<liczba;
}
void zad2 ()
{
    char znak=2;
    char *wsk_znak;
    wsk_znak=&znak;
    cout<<*wsk_znak;
}
int f_cja1(int *liczba1, int *liczba2)
{
    cout<<"Liczba 1 -f_cja1 : "<<*liczba1<<"\n";
    cout<<"Liczba 2 -f_cja1 : "<<*liczba2<<"\n";
    *liczba1=10;
    *liczba2=10;
    cout<<"Liczba 1 -f_cja1 : "<<*liczba1<<"\n";
    cout<<"Liczba 2 -f_cja1 : "<<*liczba2<<"\n";

}

