#include <iostream>

using namespace std;

class A{
    public:
        void f()
        {
            cout<<"A";
        }
        virtual void vf()
        {
            cout<<"vA";
        }
};

class B : public A{
    public:
        void f()
        {
            cout<<"B";
        }
        virtual void vf()
        {
            cout<<"vB";
        }
};

int main()
{
    A a;
    a.f();
    cout<<"\n";
    a.vf();
    cout<<"\n";
    B b;
    b.f();
    cout<<"\n";
    b.vf();


    cout<<"\n\n\n";

    return 0;
}
