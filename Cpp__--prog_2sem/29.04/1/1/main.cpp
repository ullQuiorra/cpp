#include <iostream>

using namespace std;

class Monster{
    public:
        string nazwa;
        virtual float damage()=0;
        virtual void odglos()=0;
        virtual float hp()=0;
        Monster()
        {
            nazwa="Monster";
        }
        virtual ~Monster(){};
};

class Wampir : public Monster{
    public:
        float damage()
        {
            return 72.7;
        }
        void odglos()
        {
            cout<<"\nBlooD.... !!!\n";
        }
        float hp()
        {
            return 6500;
        }
        Wampir()
        {
            nazwa="Wampir";
        }
        ~Wampir()
        {
            cout<<"\nWampir zlikwidowany!\n\n";
        }
};

class Elf : public Monster{
    public:
        float damage()
        {
            return 52.8;
        }
        void odglos()
        {
            cout<<"\nCiah ciah!!\n";
        }
        float hp()
        {
            return 4250;
        }
        Elf()
        {
            nazwa="Elf";
        }
        ~Elf()
        {
            cout<<"\nElf zlidkwidowany!\n\n";
        }
};

int main()
{
    Monster *mon;
    mon=new Wampir;
    (*mon).odglos();
    delete mon;
    mon=new Elf;
    (*mon).odglos();
    delete mon;


    return 0;
}
