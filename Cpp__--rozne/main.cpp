#include <iostream>
#include<conio.h>
#include<cstdio>

using namespace std;

class licz_zesp{
	friend licz_zesp operator+(licz_zesp a, licz_zesp b);  //zaprzyjaznienie funkcji zeby mogla korzystac z prywatnych skladowych klasy
	friend licz_zesp operator+(float a, licz_zesp b);  //zaprzyjaznienie funkcji zeby mogla korzystac z prywatnych skladowych klasy
	friend licz_zesp operator+(licz_zesp a, float b);  //zaprzyjaznienie funkcji zeby mogla korzystac z prywatnych skladowych klasy

	float Re,Im;

public:

	licz_zesp(float x, float y):Re(x),Im(y){}   //konstruktor z lista inicjalizacyjna

	licz_zesp(){                  //konstruktor domniemany
		this->Re=0;
		this->Im=0;}

	licz_zesp dodaj(licz_zesp liczba1, licz_zesp liczba2){   //metoda dodaj
		licz_zesp wynik;
		wynik.Re=liczba1.Re+=liczba2.Re;
		wynik.Im=liczba1.Im+=liczba2.Im;
		return wynik;
	}

	float wyswietlRe(){  //metoda klasy wyswietla Re ; tak trzeba bo Re w klasie jest teraz prywatne i dostep maja jedynie metody tej klasy i zaprzyjaznione funkcje
		return Re;
	}

	float wyswietlIm(){//metoda klasy wyswietla Im ; tak trzeba bo Im w klasie jest teraz prywatne i dostep maja jedynie metody tej klasy i zaprzyjaznione funkcje
		return Im;
	}

/*	licz_zesp operator+(licz_zesp b){  //funkcja operatorowa+ w wersji skladowej klasy
		licz_zesp wynik;
		wynik.Re=this->Re+b.Re;
		wynik.Im=this->Im+b.Im;
		return wynik;

	}*/




};

licz_zesp operator+(licz_zesp a, licz_zesp b){    //funkcja operatorowa+ w wersji globalnej; jest umieszczona w komentarzach bo albo ta funkcja albo ta w jako skladowa klasy
	licz_zesp wynik(a.Re+b.Re,a.Im+b.Im);
	return wynik;
}

licz_zesp operator+(float a, licz_zesp b){  //funkcja operatorowa dodajaca liczbe rzeczywista do liczby zespolonej
	licz_zesp wynik(a+b.Re,b.Im);
	return wynik;
}

licz_zesp operator+(licz_zesp a, float b){  //funckja operatorowa dodajaca liczbe zespolona do liczby rzeczywistej
	licz_zesp wynik(a.Re+b,a.Im);
	return wynik;
}




int main(void){

	licz_zesp liczba1(2,3);  //tworzenie obiektow
	licz_zesp liczba2(3,2);
	licz_zesp liczba3,liczba4;


	liczba3=liczba1+liczba2;  //testowanie funkcji operatorowej
	liczba4=liczba4.dodaj(liczba1,liczba2); //testowanie metody dodaj


	cout <<"Wynik dodawania funkcji operatorowej: "<<liczba3.wyswietlRe()<<"+"<<liczba3.wyswietlIm()<<"i";
	cout <<"\nWynik dodawania metody dodaj: "<<liczba4.wyswietlRe()<<"+"<<liczba4.wyswietlIm()<<"i";
	


getch();

}