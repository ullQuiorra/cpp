// ***************************************************
// Program z paragrafu   19.9 (str 817)
// ***************************************************

// Sprawdzony na Linuksie,    kompilator: GNU gcc version 3.3.3 (SuSE Linux)
// Sprawdzony na Windows XP,  kompilator: Microsoft Visual C++ 6.0


// Po raz pierwszy zastosowalem te sposoby w przykladzie 14_1_1.cpp

#include <conio.h>
#include <iostream>
using namespace std;


#include <string>             // strcpy
#include <ctime>               // time

void zwloka(int sekund); // sama deklaracja

// ***********************************************************
// Aby mozliwe bylo pisanie na ekranie w miejscach o okreslonych
// wspolrzednych stosuje pewien chwyt. Otoz zamiast pisac wprost
// na ekranie, - "piszemy" w obiekcie klasy string o rozmiarze [25*80].
// Takie zwykle sa monitory alfanumeryczne: maja one 80 znakow w linii,
// a na ekranie jest takich linii 25
// Funkcja skladowa o nazwie zamaluj_obszar, kasuje poprzednia tresc
// "ekranu" i zapelnia wstepnie kropkami. (Pierwotnie zapelnialem
// spacjami, ale teraz kropki podobaja mi sie bardziej).
// Dodatkowo na koncu kazdej "linii" wpisywany jest znak nowej linii.
// Ten string mozemy wypisac na ekranie jedna instrukcja - jako jeden
// tekst, ktory w okreslonych miejscach ma znaki przejscia do nowej
// linii.

// Gdy taki string jest juz zapelniony sensowna trescia, wypisujemy
// go na ekranie funkcja skladowa o nazwie wyswietl.

// Oczywiscie wszystko to ma sens tylko wtedy gdy czcionki piszace
// po ekranie sa proporcjonale - czyli gdy szerokosc wszystkich znakow
// jest taka sama. (znak 'W' jest tak samo szeroki jak znak '.').
// Tak wlasnie jest w wypadku ekranu alfanumerycznego.
// Jesli u Ciebie jest inaczej, zmien czcionke na proporcjonalna,
// (porporcjonalna jest na przyklad czcionka "Courier").
// To jest glowny powod, dla ktorego zastosowalem kropki a nie spacje -
// dzieki temu od razu zauwazysz, gdybys mial czcionke nieproporcjonalna.


// Ponizsza klasa nic nie wie o zadnych "okienkach". Potrafi tylko
// wypisac tekst w miejscu o okreslonych wspolrzednych (wzglednych)
// i ewentualnie zamalowac dany obszar kolorem (tu oszukiwanym za
// pomoca dowolnego znaku ASCII)
// (okienkami natomiast zajmuje sie inna klasa "wizerunek_ekranu").
// ***********************************************************
class ekran_alfanum  // `1
{
	string tresc;
	char tlo_ekranu;
	enum { wys_ekranu = 24, szer_ekranu = 63 };
	
public:
	ekran_alfanum() : tlo_ekranu(' ')
	{
		tresc.resize(wys_ekranu * szer_ekranu, tlo_ekranu);
	}
	//--------------------
	void zamaluj_obszar(int lewy, int gora, int prawy, int dol, char tlo_okna);
	//--------------------
	void wyswietl()
	{
		cout << tresc << flush;
	}
	void wymaz_wszystko() 
	{
		zamaluj_obszar(0,0, szer_ekranu, wys_ekranu, tlo_ekranu);     
	}
	//-------------------
	// funkcja skladowa wpisujaca w miejscu x,y zadany tekst
	void napisz(int kolumna, int rzad, string tekst)
	{
		tresc.replace(szer_ekranu * rzad + kolumna, tekst.length(), tekst);
	}	
};
//////////////////////////////////////////////////////////////
void ekran_alfanum::zamaluj_obszar(int lewy, int gora, int prawy, int dol, char tlo_okna)
{
	// wypelnienie zadanego fragmentu ekranu "kolorem" tla okna
	int dlugosc = prawy - lewy ;
	for(int y = gora ; y < dol ; y++)
	{
		tresc.replace(y * szer_ekranu + lewy, dlugosc, dlugosc, tlo_okna) ;
	}
	
	// dla pewnosci wstawienie znakow przejscia do nowej linii ekranowej
	// w odpowiednich miejscach stringu
	for(int i = 0 ; i < wys_ekranu ; i++)
	{
		tresc[(i+1) * szer_ekranu - 1] =  '\n';
	}
}
//*****************************************************************

ekran_alfanum  monitor;  // definicja globalnego obiektu `2

// ****************************************************************
class okno;          // deklaracja zapowiadajaca   

////////////////////////////////////////////////////////////////////////////
// definicja klasy odpowiadajacej za ustawianie okien na ekranie
// wzgledem siebie, wyjmowanie niektorych na wierzch itd.
// Tu jest caly "program naukowy" tego przykladu.
///////////////////////////////////////////////////////////////////////////
class wizerunek_ekranu                           //  `3
{
	okno * tab_okn[20];                // tablica wskaznikow
	int ile_okien;
public:
	// konstruktor
	wizerunek_ekranu() : ile_okien(0)
	{}
	
	void odswiez();     // narysowanie obecnego stanu ekranu
	void mazanie();     // mazanie tresci calego ekranu
	// ------------ przeladowane operatory
	void operator +=(okno & ref_ok);   // dodawanie okienka
	wizerunek_ekranu & operator +(okno & ref_ok);// dodawanie okienek
	void operator -=(okno & ref_ok);          // usuwanie okienka
	void operator != (okno & ref_ok);     // wyjmowanie na
	// sam wierzch
};
///////////////////////////////////////////////////////
class okno                                              // `4
{
	int x, y, wys, szer;     // geometria okna
	char kolor;               // kolor okienka
	string tytul;          // opis okienka
public:
	okno(int xx, int yy, int ss, int ww,
		char kk, string tt)
		: x(xx), y(yy), wys(ww), szer(ss), kolor(kk),
		tytul(tt)
	{}
	// dodanie okienka w operacji pulpit = pulpit + okienko
	wizerunek_ekranu operator +(okno & m2);
	void narysuj_sie();
};
////////////////////////////////////////////////////////
// definicje funkcji skladowych klasy okno
/******************************************************/
void okno::narysuj_sie()  // `5
{
	monitor.zamaluj_obszar(x,y, x + szer, y + wys, kolor); // zamaluj kolorem tla
	
	// narysowanie ramki - dwie linie poziome -------------
	string linijka(szer, '-');
	monitor.napisz(x, y,		linijka);
	monitor.napisz(x, y + wys,   linijka);
	
	// c. d. rysowania ramki - dwie linie pionowe------------
	for(int i = 0; i < wys+1; i++)
    {
		monitor.napisz(x, y + i, "|");
		monitor.napisz(x + szer, y+i,  "|");
    }
	
	// napisanie tytulu okna na srodku pierwszej linijki
	monitor.napisz(x + (szer - (tytul.size())) / 2, y , tytul);
	monitor.wyswietl();
}
/******************************************************/
wizerunek_ekranu okno::operator +(okno & m2)                 // `6
{
	// dodanie okienek do chwilowego pulpitu roboczego
	wizerunek_ekranu roboczy;
	roboczy += *this;        // dodajemy pierwsze (pulpit += okno)
	roboczy += m2;        // dodajemy drugie
	return roboczy;        // zwracamy pulpit (przez wartosc)
}
/*****************************************************/
// definicje funkcji skladowych klasy wizerunek_ekranu
/*****************************************************/
void wizerunek_ekranu::operator +=(okno & ref_ok)  //  dodawanie okna `7
{
	tab_okn[ile_okien++] = &ref_ok;
	odswiez();
}
/*****************************************************/
void wizerunek_ekranu::operator -=(okno & ref_okna)
{                                             // usuwanie okna `8
	// odszukanie w tablicy wskaznika do tego okna
	int i= 0;
	for( ; i < ile_okien ; i++)
    {
		if(tab_okn[i] == &ref_okna) break;
    }
	
	// sprawdzamy jak zakonczylo sie poszukiwanie
	if(i == ile_okien)
    {
		// tzn. takiego okna nie ma obecnie na ekranie
		// wiec po prostu nic nie robimy
    }
	else
    {     // okno odszukane, to je usuwamy ztablicy
		for(int k = i ; k < ile_okien ; k++)
		{      // przesuwanie pozostalych
			tab_okn[k] = tab_okn[k+1];
		}
		ile_okien--;
    }
	mazanie();     // najpierw musimy zmazac caly pulpit
	odswiez();
}
/*****************************************************/
/* wydobycie na sam wierzch zadanego okienka */
void wizerunek_ekranu::operator != (okno & ref_okna)               // `9
{
	// polega na postawieniu go na samym ko�cu tablicy
	// w tym celu najprosciej usun�� je zlisty i natychmiast doda�
	
	*this -= ref_okna;          // czyli pulpit -= okno
	*this += ref_okna;          // czyli pulpit += okno
	odswiez();
}
/*****************************************************/
void wizerunek_ekranu::odswiez()                              // `10
{
	for(int i = 0 ; i < ile_okien ; i++)
    {
		tab_okn[i]-> narysuj_sie();
    }
}
/*****************************************************/
void wizerunek_ekranu::mazanie()
{
	monitor.wymaz_wszystko() ;	
}
/******************************************************/
wizerunek_ekranu & wizerunek_ekranu::operator +(okno & ref_okna)          // `11
{                                    // dodanie okna do pulpitu
	*this += ref_okna;               // czyli pulpit += okno
	return *this;                    // czyli return pulpit
}
/******************************************************/
int main()
{
	
	wizerunek_ekranu pulpit;               // definicja obiektu pulpit
	
	// definicja kilku okienek
	okno gra(15, 7,  21, 6, '$', "Gra w Chinczyka");
	okno kalkulator(2, 3, 14, 6, '@', "Kalkulator");  
	okno edytor(7,5, 18, 6, ':', "Edytor");
	
	// umieszczenie okien na pulpicie

	pulpit = gra + kalkulator + edytor;               // `12
	zwloka(1);
	
	// wymyslamy jeszcze jedno okno
	okno zegar(4,9,18,6, '%', "Zegar");
	
	pulpit += zegar;     // dodanie go do obecnego pulpitu `13
	
	zwloka(1);
	pulpit != kalkulator; // wyjecie kalkulatora na sam wierzch  `14
	
	zwloka(1);
	pulpit != gra;          // teraz wyjecie gry
	zwloka(1);
	pulpit -= kalkulator;         // usuniecie jednego zokien ` 15
	
	zwloka(1);
	
	getch();
}
// *******************************************************************
// Dodatkowo zdefiniowalem funkcje zwloka, ktora powoduje w programie
// zwloke czasowa o dlugosci zadanej liczby sekund
// *******************************************************************
void zwloka(int sekund)
{
	static time_t poprzedni_czas = time(NULL);
	while(time(NULL) - poprzedni_czas < sekund);   // cialo puste
	
	poprzedni_czas = time(NULL);
}


/************************************************************
			  
************************************************************/

