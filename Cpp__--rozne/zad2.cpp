#include <iostream>
#include<conio.h>
//#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<cassert>

using namespace std;
 
 class wizytowka
 {
 public:
	 char*nazw;
	 char*imie;
	 char*tel;

//////////////////////[ konstruktor domniemany ]//////////////////////////////////////////////
	 wizytowka(const char*N="puste",const char*I="puste",const char*T="puste")
	 {
		 cout<<"Konstruktor"<<endl;
		 nazw=new char[strlen(N)+1];
			assert(nazw!=0);
			strcpy(nazw,N);

		imie=new char[strlen(I)+1];
			assert(imie!=0);
			strcpy(imie,I);

		tel=new char[strlen(T)+1];
			assert(tel!=0);
			strcpy(tel,T);
	 }
	 
//////////////////////[ dekonstruktor ]//////////////////////////////////////////////
	 ~wizytowka()
	 {
		 cout<<"Destruktor"<<endl;
		 if(nazw)
			 delete[]nazw;
		 if(imie)
			 delete[]imie;
		 if(tel)
			 delete[]tel;
	 }

	 void wypisz()
	 {
		 cout<<nazw<<", "<<imie<<", "<<tel<<endl;
	 }

//////////////////////[ konstruktor kopiuj�cy ]//////////////////////////////////////////////
	 wizytowka(const wizytowka &w)
	 {
		cout<<"konstruktor kopiujacy"<<endl;
		 nazw=new char[strlen(w.nazw)+1];
		 assert(nazw!=0);
		 strcpy(nazw,w.nazw);

		imie=new char[strlen(w.imie)+1];
		assert(imie!=0);
		strcpy(imie,w.imie);

		tel=new char[strlen(w.tel)+1];
		assert(tel!=0);
		strcpy(tel,w.tel);
	 }

//////////////////////[ operator przypisania ]//////////////////////////////////////////////
	 wizytowka &operator=(const wizytowka &w)
	 {
		 cout<<"Operator przypisania"<<endl;
		 if(&w==this)return *this;
		 if(nazw)delete []nazw;
		 if(imie)delete []imie;
		 if(tel)delete []tel;

		 
		 if(w.nazw)
		 {
		nazw=new char[strlen(w.nazw)+1];
		assert(nazw!=0);
		strcpy(nazw,w.nazw);
		 }
		  if(w.imie)
		 {
		imie=new char[strlen(w.imie)+1];
		assert(imie!=0);
		strcpy(imie,w.imie);
		 }
		   if(w.tel)
		 {
		tel=new char[strlen(w.tel)+1];
		assert(tel!=0);
		strcpy(tel,w.tel);
		   }
		   return *this;
	 }
	
 };

///////////////////////////////////////////////////
 void pisz(wizytowka w)
 {
	 w.wypisz();
 }
 int main()
 {

	wizytowka w1("Krzysztof","Paszuk","515..");
	wizytowka w2("N","K","7");
	wizytowka w3;
	w1.wypisz();
	w2.wypisz();
	w3.wypisz();
	cout<<endl;
	w2=w1;
	w1.wypisz();		//operator przypisania
	w2.wypisz();
	cout<<endl;
	cout<<"przed pisz"<<endl;
	pisz(w2);
	cout<<"po pisz"<<endl;




getch();

return 0;
 }
