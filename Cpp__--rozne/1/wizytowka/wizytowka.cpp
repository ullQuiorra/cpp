#include <iostream>
#include <conio.h>
#include <cstdio>
#include <cstdlib>
#include <cassert>

using namespace std;

class wizytowka
{
	public:
	char *nazwisko,
			*imie,
			*telefon,
			*adres;
	///kostruktor domniemany
		wizytowka(char *N="puste",char *I="puste",char *T="puste",char *A="puste")
		{
			cout<<"konstruktor"<<endl;

			nazwisko=new char[strlen(N)+1];
				assert(nazwisko!=0);			//sprawdza pamie�
				strcpy(nazwisko,N);

			imie=new char[strlen(I)+1];
				assert(imie!=0);
				strcpy(imie,I);

			telefon=new char[strlen(T)+1];
				assert(telefon!=0);
				strcpy(telefon,T);

			adres= new char[strlen(A)+1];
				assert(adres!=0);
				strcpy(adres,A);
		}
	
	void wypisz()
	{
		cout<<nazwisko<<", "<<imie<<", "<<telefon<<", "<<adres<<endl;
	}

///////////[ dekonstruktor ]////////////////////////////////////////////////////////////

	~wizytowka()
	{
		if (nazwisko) delete[]nazwisko;
		if (imie)	delete[]imie;
		if (telefon) delete[]telefon;
		if (adres) delete[]adres;
	}
////////////[ konstruktor kopiujacy ]////////////////////////////////////////////////////////////	
	
	wizytowka(const wizytowka &w)
	{
		cout<<"konstruktor kopiuj�cy"<<endl;

		nazwisko=new char[strlen(w.nazwisko)+1];
			assert(nazwisko!=0);
			strcpy(nazwisko,w.nazwisko);

		imie =new char[strlen(w.imie)+1];
			assert(imie!=0);
			strcpy(imie,w.imie);

		telefon=new char[strlen(w.telefon)+1];
			assert(telefon!=0);
			strcpy(telefon,w.telefon);

		adres=new char[strlen(w.adres)+1];
			assert(adres!=0);
			strcpy(adres,w.adres);
	}


	wizytowka &operator=(const wizytowka &w)
	{
	cout<<"operator przypisania"<<endl;

	if(&w==this) return *this;
	if(nazwisko) delete []nazwisko;
	if(imie) delete [] imie;
	if (telefon) delete [] telefon;
	if (adres) delete []adres;


	if (w.nazwisko)
	{
		nazwisko=new char[strlen(w.nazwisko)+1];
		assert(nazwisko!=0);
		strcpy(nazwisko,w.nazwisko);
	}
	
	if (w.imie)
	{
		imie=new char[strlen(w.imie)+1];
		assert(imie!=0);
		strcpy(imie,w.imie);
	}

	if (w.telefon)
	{
		telefon=new char[strlen(w.telefon)+1];
		assert(telefon!=0);
		strcpy(telefon,w.telefon);
	}

	if (w.adres)
	{
	
		adres=new char[strlen(w.adres)+1];
		assert(adres!=0);
		strcpy(adres,w.adres);
	}

	return *this;



	}

	


};

void pisz(wizytowka w)
{
	w.wypisz();
}

int main()
{
	wizytowka w1("Krzysztof","Paszuk","515625","Podlesie");
	wizytowka w2("Jan","Nowak","123456","Warszawa"),w3,w4;
	

	w1.wypisz();
	w2.wypisz();
	w3.wypisz();
	w4.wypisz();
	cout<<endl;


	w3=w2;			//op. kopiuj�cy
	w3.wypisz();
	cout<<endl;


	w4=w1;
	pisz(w4);



	getch();
	return 0;
}