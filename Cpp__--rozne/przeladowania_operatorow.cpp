// przeladowania_operatorow.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>


using namespace std;

class ulamek
{
	private:
		int L, M;

	public:
		ulamek(int nL, int nM)
		{
			L=nL;
			M=nM;
		};


		void wypisz()
		{
			cout<<L<<endl<<"-"<<endl<<M<<endl;
		};

		friend ulamek operator+( ulamek &u1, ulamek &u2);
};

ulamek operator+( ulamek &u1, ulamek &u2)
{
	ulamek wynik(0,0);
	if (u1.M != u2.M)
	{
		
		wynik.L= u1.L*u2.M+u2.L*u1.M;
		wynik.M= u1.M*u2.M;

	}   else
		{
			wynik.L=u1.L+u2.L;
			wynik.M=u1.M;
		}


	return wynik;

}
	   


int _tmain(int argc, _TCHAR* argv[])
{

	ulamek u1(2,2);
	ulamek u2(3,3);
	ulamek wynik(0, 0);
	

	wynik=u1+u2;
	wynik.wypisz();
	getchar();
	return 0;
}

